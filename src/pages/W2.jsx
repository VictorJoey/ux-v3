import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'


function W2() {
  return (
    <div>
<Header />
<section className="blog-main-section">
  <div className="page-padding">
    <img
      alt=""
      loading="lazy"
      width={54}
      src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940b1d7186680090debea9_logo2.png"
    />
    <h1 className="blog-main-heading">
      Navigating UK Home Office RTW Policy Changes - Orka
    </h1>
    <p>
      Back in September 2022, the Home Office shook things up by revising its
      digital Right-to-Work check guidelines. This threw a curveball at users of
      Orka Works who relied on the app for their employment verification. I
      found myself leading a critical project to revamp Orka's identification
      process. The catch? We had to move fast. Recognizing the urgency, I worked
      on weaving the new guidelines into a single, more efficient platform that
      would tick all the compliance boxes. This not only made life easier for
      our users but also helped us sail through an audit. It was a win-win, and
      Orka Check emerged as a robust standalone vetting platform.
    </p>
    <div className="_3rd-role-others">
      <div id="w-node-e4c68ba1-6056-4f8f-ac8c-aef8e97a1b46-c3027020">
        <div className="blog-tag">
          <strong>Role</strong>
        </div>
        <div>Lead Product Designer</div>
      </div>
      <div id="w-node-_661e2754-89ad-753a-1dc3-d16fd786189d-c3027020">
        <div className="blog-tag">
          <strong>Timeline</strong>
        </div>
        <div>Tight three-month, from Sept 2022 to Jan 2023.</div>
      </div>
      <div id="w-node-_1ed84d76-4df9-7979-bebf-a67c7c90f934-c3027020">
        <div className="blog-tag">
          <strong>Core Responsibilities</strong>
        </div>
        <div>
          Product lead, product design, user research, system thinking, product
          strategy, agile, prototyping, Iterative User-Centered Design
        </div>
      </div>
    </div>
  </div>
  <div className="top-img-2">
    <div id="w-node-_111bbfaf-4893-b7a5-dafb-f33bd4acca1e-c3027020">
      <img
        alt=""
        loading="lazy"
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933d3287cc70dbec6da8ce_65932a832a3b2740919fd014_Group%252010.png"
      />
    </div>
    <div id="w-node-_5418390a-1d2b-7caf-a7fb-554472d2adee-c3027020">
      <img
        alt=""
        loading="lazy"
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933d3287cc70dbec6da8cb_65932a6ad4215558dc1fd6f6_13.png"
      />
    </div>
  </div>
  <div className="page-padding">
    <div className="all-cont">
      <div className="left-menu">
        <div
          data-animation="default"
          data-collapse="medium"
          data-duration={400}
          data-easing="ease"
          data-easing2="ease"
          role="banner"
          className="navbar-2 w-nav"
        >
          <div className="text-block-4 overview">Overview</div>
          <div className="container-2 w-container">
            <nav role="navigation" className="blog-menu w-nav-menu">
              <a
                href="w2#problemms"
                className="nav-link w-nav-link"
              >
                Problem
              </a>
              <a
                href="w2#goals"
                className="nav-link w-nav-link"
              >
                Goals
              </a>
              <a
                href="w2#Solutions-section"
                className="nav-link w-nav-link"
              >
                Solution
              </a>
              <a
                href="w2#Results-section"
                className="nav-link w-nav-link"
              >
                Results
              </a>
              <div className="wrapper dfdf" />
              <p className="paragraph-18">Additional Context</p>
              <a
                href="w2#reasrch-section"
                className="nav-link w-nav-link"
              >
                Research
              </a>
              <a
                href="w2#xfn-collaboration"
                className="nav-link w-nav-link"
              >
                Xfn Collaboration
              </a>
              <a
                href="w2#Ideation"
                className="nav-link w-nav-link"
              >
                Ideation
              </a>
              <a
                href="w2#validation"
                className="nav-link w-nav-link"
              >
                Validation
              </a>
              <a
                href="w2#plan-of-action"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">Plan of Action</strong>
              </a>
              <a
                href="w2#strategicphase3-5"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">strategic phase 3 of 5</strong>
              </a>
              <a
                href="w2#strategicphase4-5"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">strategic phase 4 of 5</strong>
              </a>
              <a
                href="w2#Future-steps"
                className="nav-link w-nav-link"
              >
                Future steps
              </a>
              <a
                href="w2#reflection"
                className="nav-link w-nav-link"
              >
                Reflection
              </a>
            </nav>
          </div>
        </div>
      </div>
      <div className="rigth-contt">
        <div id="problemms" className="problemms">
          <div className="w-richtext">
            <p>Problem &amp; Insights</p>
            <h4>
              <strong>
                Understanding the impact of recent Home Office right to work
                policy changes on Orka's interconnected platforms
                <br />
              </strong>
            </h4>
            <p>
              In collaboration with the product design and product manager
              teams, we started our evaluation with User Journey Mapping to
              analyze the current workflows in Orka Work. This led to a
              Heuristic Evaluation and a Cognitive Walkthrough, focusing on the
              impact on Orka Work's functionality and the complexities arising
              from the intertwined checks in Orka Work and Orka Check.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1944pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65921802139e1daa37fa473d_flows.png"
                  alt=""
                />
              </div>
            </figure>
            <p>
              <strong>
                Complexities in aligning Orka Works and Orka Check
              </strong>
            </p>
            <p>
              Understanding the complexity of implementing these changes. I
              adopted a Systems Thinking approach to see the bigger picture.
              This helped us see how the Works App and the Check platform were
              interconnected and where the fundamental problems lay.
            </p>
          </div>
        </div>
        <div id="problemms" className="problemms">
          <div className="top--flexx">
            <div>
              <img
                alt=""
                loading="lazy"
                src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940c68d4eb33d890476bab_HOME%20OFFICE.png"
                sizes="100vw"
                srcSet="
                https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940c68d4eb33d890476bab_HOME%20OFFICE-p-500.png   500w,
                https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940c68d4eb33d890476bab_HOME%20OFFICE-p-800.png   800w,
                https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940c68d4eb33d890476bab_HOME%20OFFICE-p-1080.png 1080w,
                https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940c68d4eb33d890476bab_HOME%20OFFICE.png        1512w
              "
              />
            </div>
            <div>
              <img
                alt=""
                loading="lazy"
                src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940c86f9e5814f58c52459_ORKA%20WORKS%20CHECK.png"
                sizes="100vw"
                srcSet="
                https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940c86f9e5814f58c52459_ORKA%20WORKS%20CHECK-p-500.png   500w,
                https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940c86f9e5814f58c52459_ORKA%20WORKS%20CHECK-p-800.png   800w,
                https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940c86f9e5814f58c52459_ORKA%20WORKS%20CHECK-p-1080.png 1080w,
                https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940c86f9e5814f58c52459_ORKA%20WORKS%20CHECK.png        1512w
              "
              />
            </div>
          </div>
        </div>
        <div id="goals" className="goall-section">
          <div className="w-richtext">
            <p>Goals</p>
            <h4>
              <strong>
                Orka's right-to-work check process needed to evolve because the
                previous approach had become non-functional
              </strong>
            </h4>
            <p>
              The Home Office changes, and workers at Orka navigated a complex,
              multi-stage journey. This began with signing up for Orka Works,
              followed by Right-to-Work checks, app onboarding, background
              checks via Orka Check, and culminating in starting their work.
              Despite being operational, this fragmented process caused user
              frustration and delays. Through collaborative efforts with our
              Finance, Growth, and Compliance teams, we embarked on a dual
              mission: swift enhancements to both the Orka Works and Orka Check
              apps and the search for a new Right-to-Work Check partner.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592188dcd940a3b00f21b76_Wprks.png"
                  alt=""
                />
              </div>
            </figure>
            <p>Reduce friction on Orka Works onboarding screen</p>
            <h4>
              <strong>
                Enable users to start working more quickly and easily,
                benefiting both them and the businesses that rely on the app.
              </strong>
            </h4>
            <p>
              Our goal was to simplify the onboarding process, ensuring peace of
              mind for workers and effortless work experience. These
              improvements would result in increased engagement with the
              onboarding process, higher signup rates, improved user retention,
              an enhanced reputation, and the attraction of new users.
              Additionally, it would enable users to start working more quickly
              and easily, benefiting both them and the businesses that rely on
              the app.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659218c5a52c8722dc7c9144_Checks.png"
                  alt=""
                />
              </div>
            </figure>
            <p>All the checks are in one place</p>
            <h4>
              <strong>
                Users benefit from reduced complexity as Orka Check becomes an
                independent vetting platform
              </strong>
            </h4>
            <p>
              Orka Check has developed a self-contained vetting platform that
              encompasses both the right-to-work and right-to-check processes.
              This comprehensive approach ensures users can seamlessly onboard
              and promptly commence their work. This holistic approach
              streamlines user onboarding, ensuring a seamless and efficient
              experience. Users benefit from reduced complexity, quicker
              commencement of work, and a user-friendly interface, enhancing
              overall satisfaction.
            </p>
            <p>‍</p>
            <h4>
              <strong>
                On the other hand, Significant challenges with our existing ID
                verification partner, Onfido, prompted a critical reevaluation
                of our partnership
              </strong>
            </h4>
            <p>
              In a somewhat déjà vu moment, I drew upon my previous experience
              with advanced ID verification solutions and realized that our
              existing ID verification partner, Onfido, might no longer be the
              ideal fit for our needs. Our analysis of Onfido revealed several
              key issues. Firstly, it predominantly focused on document-based
              verification, which didn't align with the diverse range of
              verification scenarios we encountered, thus highlighting a
              significant mismatch. Secondly, Onfido operated as a web-based
              platform, posing challenges for our predominantly mobile-oriented
              user base, and creating friction in the verification process.
              Lastly, and critically, its verification procedures failed to
              align with the updated standards set by the Home Office,
              indicating a lack of regulatory compliance &nbsp;— &nbsp;a vital
              factor in our industry.
            </p>
          </div>
        </div>
        <div id="Solutions-section" className="solutions-section">
          <div className="w-richtext">
            <p>Solution</p>
            <h4>
              <strong>
                A comprehensive four-step design strategy was implemented to
                align Orka's processes with the latest Right-to-Work check
                guidelines, ensuring compliance and efficiency
              </strong>
            </h4>
            <p>
              <strong>Step: 1 </strong>
            </p>
            <p>
              The first step involved scouting for a new partner that could
              offer advanced and compliant Right-to-Work check services,
              aligning with the latest Home Office standards.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "972pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592197b50e3623a1049eb23_solutions.png"
                  alt=""
                />
              </div>
            </figure>
            <p>
              During this race against the clock, I took on a multifaceted role,
              encompassing leadership within the design team and steering our
              overarching product strategy. My responsibilities spanned
              conducting comprehensive research and user interviews, guiding the
              development of visual and interaction design elements, and
              ensuring seamless coordination with our front-end development
              team.
            </p>
            <p>
              <strong>Step: 2 </strong>
            </p>
            <p>
              Integrating the new Right-to-Work check as the initial step in the
              Orka Check Candidate platform was crucial. This integration
              ensured a seamless and efficient start to the user’s verification
              process
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659219c5a52c8722dc7d1d03_Orka%20Works%20candidate.png"
                  alt=""
                />
              </div>
            </figure>
            <p>
              <strong>Step: 3 </strong>
            </p>
            <p>
              ⁠Reflect Changes on Orka Check Screening Portal: Updating the Orka
              Check Screening Portal to mirror these new procedural changes.
              This was essential to maintain consistency and clarity across all
              user touch points.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "972pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65921a010f76be8f773661d7_Step_%203.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Results-section" className="results-section">
          <div className="w-richtext">
            <p>Results</p>
            <h4>
              <strong>
                100% compliance with updated home office standards, 30%
                reduction in onboarding time, 20% reduction in verification
                costs, and cross-functional team reported 15% higher job
                satisfaction
              </strong>
            </h4>
            <p>
              Orka's Right-to-Work Check transformation resulted in improved
              user satisfaction, faster onboarding, an enhanced reputation, and
              cost savings. Achieving full compliance with updated Home Office
              standards ensured regulatory adherence, while an increase in user
              registrations indicated platform appeal. Data integrity remained
              intact, scalability increased significantly, and internal teams
              reported higher job satisfaction. Notably, Orka successfully
              passed the audit, further solidifying its competitive edge in the
              HR tech industry
            </p>
            <p>‍</p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65921b40139e1daa37fc3871_solutions.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div className="wrapper" />
        <div id="reasrch-section" className="reasrch-section">
          <div className="w-richtext">
            <p>Research</p>
            <h4>
              <strong>
                Understanding the adverse effects of the Home Office's digital
                Right-to-Work check modifications on vulnerable hourly workers
                and the cost of living crisis.
              </strong>
            </h4>
            <p>
              In September, the Home Office modified the digital Right-to-Work
              check process. Pre-COVID, these checks required physical document
              exchanges, but during the pandemic, video calls were temporarily
              allowed for verification. Post-COVID, the Home Office reverted to
              the original method, creating two distinct groups for
              verification: those with UK or Irish passports or a Home Office
              share code can still use digital checks, while those without these
              documents must provide physical documentation or visit in person.
              This change significantly impacts vulnerable job seekers,
              influencing the focus of my project at Orka to develop an
              inclusive, compliant verification system.
            </p>
            <h4>
              <strong>
                Discontinuing the successful digital checking method after 26
                months adds hurdles for Orka Works users during a cost of living
                crisis.
              </strong>
            </h4>
            <p>
              The withdrawal of the digital checking method, previously
              successful for 26 months, adds a significant obstacle for the
              thousands of workers using Orka Works amid a cost of living
              crisis, compounding the struggles of hourly workers.
            </p>
          </div>
        </div>
        <div id="xfn-collaboration" className="reasrch-section asdas">
          <div className="w-richtext">
            <p>Xfn Collaboration</p>
            <h4>
              <strong>
                Led a dynamic team to redesign the identity verification process
                in a challenging three-month timeframe, taking on multiple roles
                from research to design and coordination.
              </strong>
            </h4>
            <p>
              Within a challenging three-month timeframe spanning from September
              2022 to January 2023, I assumed the role of project lead
              responsible for designing the identity verification process.
              Collaborating with a proficient UX designer, three adept
              engineers, and a seasoned Product Manager, we formed a dynamic
              team to address our time-sensitive objectives.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65921afa50e3623a104ac365_Xfn%20Collaboration.png"
                  alt=""
                />
              </div>
            </figure>
            <p>‍</p>
            <p>
              During this race against the clock, I took on a multifaceted role,
              encompassing leadership within the design team and steering our
              overarching product strategy. My responsibilities spanned
              conducting comprehensive research and user interviews, guiding the
              development of visual and interaction design elements, and
              ensuring seamless coordination with our front-end development
              team.
            </p>
          </div>
        </div>
        <div id="Ideation" className="reasrch-section asdas">
          <div className="w-richtext">
            <p>Strategic Phase 1 of 5</p>
            <p>
              <strong>Identifying a Partner</strong>
            </p>
            <h4>
              <strong>
                Proposing the implementation of a partnership with a more
                advanced Right-to-Work Check provider.
              </strong>
            </h4>
            <p>
              With time ticking away, we knew we had to make a bold move.
              Recognizing the limitations of our current ID verification system
              with Onfido, we saw an opportunity for significant improvement. We
              proposed the idea of partnering with a new, more advanced
              Right-to-Work Check provider.
            </p>
            <h4>
              <strong>
                Enhancing our operational efficiency while ensuring strict
                adherence to regulatory demands.
              </strong>
            </h4>
            <p>
              This proposal was presented to our CEO, followed by a
              collaborative meeting with the finance and compliance teams. Our
              goal was to find a partner that offered a broader spectrum of
              verification capabilities, was more in tune with our mobile user
              base, and fully compliant with the updated Home Office standards.
              This shift was aimed at enhancing our operational efficiency while
              ensuring strict adherence to regulatory demands.
            </p>
            <h4>
              <strong>
                After careful consideration of our requirements and conducting a
                thorough analysis, we have narrowed our options to two potential
                integration partners
              </strong>
            </h4>
            <p>
              We outlined our requirements for a partner, emphasizing liveliness
              checks, mobile functionality, security, compliance, integration,
              and cost-effectiveness. After a swift evaluation, we narrowed down
              our choices to Veriff and Yoti.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "972pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65921c674d6df02a5f2d7a7f_solutiodns.png"
                  alt=""
                />
              </div>
            </figure>
            <h4>
              <strong>
                Narrowing it further with conducting evaluations, reviewing
                proposals, and considering data privacy, we selected an
                integration partner
              </strong>
            </h4>
            <p>
              In a two-day process, we carefully reviewed proposals from both
              companies, considering technology, accuracy, user-friendliness,
              security, scalability, and cost. Practical tests with real data
              confirmed their performance. We also factored in data privacy,
              customer support, and reputation, ultimately choosing Yoti for
              their advanced verification and alignment with our goals.
            </p>
            <h4>
              <strong>
                Following our meetings with Yoti, we successfully streamlined
                verification processes, enhanced compliance measures, and
                elevated the overall user experience
              </strong>
            </h4>
            <p>
              Our collaboration with Yoti started with detailed discussions
              between their team and ours, focusing on integration and
              implementation. This step streamlined verification, boosted
              compliance, and improved user experience. Once we had Yoti on
              board I had a couple of meetings with Yoti team along with the
              engineers to understand the integration and implementation.
            </p>
          </div>
        </div>
        <div id="validation" className="reasrch-section asdas">
          <div className="w-richtext">
            <p>Strategic Phase 2 of 5</p>
            <p>
              <strong>Streamline Orka Works Onboarding</strong>
            </p>
            <h4>
              <strong>Iterate Iterate Iterate</strong>
            </h4>
            <p>
              I implemented an agile and iterative approach to design and
              prototype the system. Initially, I developed the initial design
              set and conducted workshops involving cross-functional teams to
              gain consensus. After several rounds of iterations and
              refinements, we reached a final design.
            </p>
            <h4>
              <strong>
                This streamlined process benefits users by saving time and
                reducing complexity.
              </strong>
            </h4>
            <p>
              Previously, users would perform Right-to-Work (RTW) checks once
              and then proceed to complete other sections before conducting a
              background check.
            </p>
            <p>
              Now, after users sign up and complete the first step, they are
              seamlessly redirected to conduct all necessary checks on the Orka
              Check platform. Furthermore, before users can add their SIA
              License within the Orka Works App, they are guided to the Orka
              Check Platform, ensuring a more efficient and streamlined user
              experience.
            </p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592188dcd940a3b00f21b76_Wprks.png"
                  alt=""
                />
              </div>
            </figure>
            <p>Reduce friction on Orka Works onboarding screen</p>
          </div>
        </div>
        <div id="plan-of-action" className="reasrch-section asdas">
          <div id="strategicphase3-5" className="w-richtext">
            <p>Strategic Phase 3 of 5</p>
            <p>
              <strong>Adding new features to the Orka Check Platform</strong>
            </p>
            <h4>
              <strong>
                Refining and improving the design based on user feedback and
                data analysis to ensure a user-friendly and consistent
                experience.
              </strong>
            </h4>
            <p>
              Upon a thorough examination of tracking information and user
              satisfaction metrics for the<strong>Orka Check Platform</strong>,
              I leveraged existing UI elements and UX patterns to seamlessly
              integrate the Right-to-Work (RTW) check.
            </p>
            <p>
              The design process entailed several iterations to ensure a refined
              and user-friendly dashboard page for Orka Check, ultimately
              resulting in the final design. This meticulous approach aimed to
              enhance the user experience while maintaining consistency with
              established design elements and patterns.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659219c5a52c8722dc7d1d03_Orka%20Works%20candidate.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
          <div id="strategicphase4-5" className="rich-text-block-7 w-richtext">
            <p>Strategic Phase 4 of 5</p>
            <p>
              <strong>Successful integration </strong>
            </p>
            <h4>
              <strong>
                Integration of Yoti for Right-to-Work (RTW) verification within
                the Orka Check platform
              </strong>
            </h4>
            <p>
              Upon a thorough examination of tracking information and user
              satisfaction metrics for the<strong>Orka Check Platform</strong>,
              I leveraged existing UI elements and UX patterns to seamlessly
              integrate the Right-to-Work (RTW) check.
            </p>
            <p>
              The design process entailed several iterations to ensure a refined
              and user-friendly dashboard page for Orka Check, ultimately
              resulting in the final design. This meticulous approach aimed to
              enhance the user experience while maintaining consistency with
              established design elements and patterns.
            </p>
            <p>‍</p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940db532571becf2844961_Untitled.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940dbc1778903621ac6d7a_Untitled%20(1).png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
            <h4>
              <strong>
                Mandating photo uploads of SIA licenses not only bolsters the
                verification process but also ensured higher accuracy,
                compliance, and overall trustworthiness of the platform,
                benefitting both users and the organisation.
              </strong>
            </h4>
            <p>
              <strong>‍</strong>In addition to the Yoti integration, we also
              introduced a significant change by making it mandatory for both
              existing and new users to upload photos of their SIA licenses.
              Previously, users were only required to provide license
              information. This enhancement ensures greater accuracy and
              compliance.
            </p>
            <p>‍</p>
            <p>
              ‍<strong>Before</strong>
            </p>
            <p>
              <strong>‍</strong>With just SIA license number
            </p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940dfdaa8939066cca6b12_Untitled%20(2).png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
            <p>‍</p>
            <p>
              <strong>After</strong>
            </p>
            <p>
              <strong>‍</strong>Continued the existing flow and added clear
              instruction on how to add photos after SIA License
            </p>
            <p>‍</p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940e19730ac2c26108e6b0_Untitled%20(3).png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
            <p>
              A
              <strong>
                dding new licenses contributes to a more robust and reliable
                verification process, promoting accuracy, compliance, and user
                trust, while also enhancing security.
              </strong>
            </p>
            <p>
              <strong>‍</strong>Before this change, users could only add
              licenses by providing license information. However, after the
              update, users are now mandated to upload photos of their SIA
              licenses when adding new licenses, ensuring a more robust
              verification process.
              <br />
              <br />
            </p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940e8bcf0cae594e48b01e_Untitled%20(4).png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
            <h4>
              Preview of the card front and back
              <br />
              <br />
              <br />
              <strong>
                Reflecting the Orka Check Platform changes to the Orka Check
                Screeners’ Side
              </strong>
            </h4>
            <p>
              <strong>‍</strong>Compliance with Home Office regulations
              necessitated the maintenance of comprehensive records pertaining
              to the Right-to-Work (RTW) and Liveliness checks, inclusive of the
              RTW document
              <br />‍
            </p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940fdd431662c27fa23957_orka%20check.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
            <p>
              ‍<br />
              Right to check and ID verification screen
            </p>
            <p>‍</p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940fe61d741bf69da89ae1_orka%20check2.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
          <div className="rich-text-block-7 w-richtext">
            <p>Strategic Phase 5 of 5</p>
            <p>
              <strong>Development and Deployment: The final stage</strong>
            </p>
            <h4>
              <strong>
                Collaboration with the development team and the incorporation of
                advanced technology contribute to the efficient, innovative, and
                user-centric development of our product.
              </strong>
            </h4>
            <p>
              We worked hand in hand with our talented development team to
              transform our visionary design into a reality. This involved
              harmoniously integrating the design with the necessary APIs and
              systems, leveraging state-of-the-art technology to achieve
              exceptional functionality.
            </p>
            <p>‍</p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659410f5730ac2c2610ad083_reviewsorka.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Future-steps" className="reasrch-section asdas">
          <div className="w-richtext">
            <p>Future steps</p>
            <h4>
              <strong>
                Create an onboarding process that is intuitive, efficient, and
                tailored to the needs of our users.
              </strong>
            </h4>
            <p>
              Although we've made significant strides, only 30% of users
              complete the onboarding tour, indicating an area ripe for
              enhancement. I will initiate a thorough overhaul of the Orka Works
              sign-up and onboarding process. This will involve redesigning the
              user journey to prioritize simplicity and user-friendliness. I'll
              employ user-centric design principles, conduct usability studies,
              and engage with stakeholders to gather input and insights. The
              goal is to create an onboarding process that is intuitive,
              efficient, and tailored to the needs of our users.
            </p>
            <h4>
              <strong>
                Establishing a User Feedback Loop and actively engaging with our
                user community through surveys, user interviews, and testing
                sessions.
              </strong>
            </h4>
            <p>
              I will establish a User Feedback Loop to ensure ongoing
              communication with our user community. This will involve
              soliciting feedback through surveys, user interviews, and user
              testing sessions. This invaluable input will guide the iterative
              refinement of Orka Check and the onboarding process, ensuring that
              they evolve in alignment with user preferences and needs
            </p>
          </div>
        </div>
        <div id="reflection" className="reasrch-section asdas">
          <div className="w-richtext">
            <p>Reflection</p>
            <h4>
              <strong>
                Even in the face of daunting challenges, perseverance,
                innovation, and collaboration can lead to transformative
                achievements
              </strong>
            </h4>
            <p>
              This journey has been a rollercoaster of emotions, challenges, and
              triumphs. When I first took on the role of a product designer for
              this critical project, I'll admit I felt a mixture of excitement
              and apprehension. The tight three-month timeline seemed like an
              insurmountable mountain to climb, and the pressure of ensuring
              Home Office compliance weighed heavily on my shoulders. There were
              moments when I doubted whether we could complete this ambitious
              task in time. The Home Office compliance standards are stringent,
              and any misstep could have serious consequences. But that
              pressure, as daunting as it was, also served as a driving force.
              It pushed me to explore new horizons, challenge the status quo,
              and innovate at every turn.
            </p>
            <p>
              One of the most transformative parts of this journey was our
              collaboration with Yoti. Those meetings were more than just
              business transactions; they were opportunities for growth and
              learning. I had the privilege of witnessing firsthand how advanced
              identity verification solutions worked, and it opened my eyes to a
              world of possibilities. The evolution of our partnership with Yoti
              not only streamlined our verification process but also broadened
              my understanding of cutting-edge technology and its potential
              impact. When we passed the audit, it was a moment of sheer joy and
              pride. The positive shift in our company culture was palpable. It
              became evident that my role as a designer had transcended its
              traditional boundaries. I was no longer just designing interfaces;
              I was shaping the future of our organization, ensuring it stayed
              ahead in a rapidly evolving landscape. However, there were moments
              that took me by surprise, like when the other designer left the
              project. It was a shock, and I won't deny that I felt a sense of
              uncertainty. But I knew we couldn't afford to slow down. So, I
              took a deep breath and kept the momentum going. It was a test of
              my resilience, and I'm proud of how we persevered.
            </p>
            <p>
              Understanding the complexities of our interconnected platforms
              filled me with a sense of accomplishment. It was like solving a
              complex puzzle, piece by piece. Every breakthrough, every insight
              gained from the research and design process, was a source of
              immense pride. I realized that even the most intricate challenges
              could be overcome with dedication and teamwork. Speaking of
              teamwork, that was one of the highlights of this journey. Working
              with a diverse group of individuals, each with their unique
              perspectives and ideas, was incredibly enriching. Listening to the
              wealth of inputs and ideas from various team members was not only
              enlightening but also humbling. It reinforced the belief that the
              best solutions emerge when we collaborate and embrace diversity.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65921ebfbb045ff20817e062_Reflection.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<Footer />


    </div>
  )
}

export default W2