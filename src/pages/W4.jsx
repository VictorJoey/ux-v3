import React from 'react' 
import Header from '../components/Header'
import Footer from '../components/Footer'

function W4() {
  return (
    <div>
<Header />
<section className="blog-main-section">
  <div className="page-padding">
    <img
      alt=""
      loading="lazy"
      width={51}
      src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933cdf8583e7181316ecde_659327a01d30e3c3c8952c60_design%2520system%2520logo.png"
      className="blog-tag"
    />
    <h1 className="blog-main-heading">
      The Orka Design System: Bridging Platforms, Uniting Experiences
    </h1>
    <p>
      To unify user experiences across Orka Works, Orka Check, and Orka Pay by
      addressing design inconsistencies, inefficiencies, and accessibility gaps
    </p>
    <div className="_3rd-role-others">
      <div id="w-node-a5dc5c57-ce05-8af1-1355-8b04a9a07b9a-9d5f6c85">
        <div>
          <strong>Role</strong>
        </div>
        <div>Sole Product Designer</div>
      </div>
      <div id="w-node-_4c4b3633-94c6-12e5-0b75-5cdba779ef0b-9d5f6c85">
        <div>
          <strong>Timeline</strong>
        </div>
        <div>7 months, from Mar 2022 to Dec 2022</div>
      </div>
      <div id="w-node-_329bfa44-71a5-2bcb-6439-1deea17b62ee-9d5f6c85">
        <div>
          <strong>Core Responsibilities</strong>
        </div>
        <div>
          My responsibilities included forging meaningful collaborations,
          driving design innovation, and ensuring that each product within the
          Orka portfolio resonated with our unified design ethos.
        </div>
      </div>
    </div>
  </div>
  <div className="top-img-2">
    <div id="w-node-be57443c-6033-6f2f-f220-5a05684b64fe-9d5f6c85">
      <img
        alt=""
        loading="lazy"
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933cdf8583e7181316ece5_659327d8237f27f93ff0cb7b_design%2520ssystem.png"
      />
    </div>
    <div id="w-node-_3a659725-ef38-e4fa-d2ad-67636f2c4f8e-9d5f6c85">
      <img
        alt=""
        loading="lazy"
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933cdf8583e7181316ecd9_659327f67780e23c50e61ea3_Building%2520Blocks.png"
      />
    </div>
  </div>
  <div className="page-padding">
    <div className="all-cont">
      <div className="left-menu">
        <div
          data-animation="default"
          data-collapse="medium"
          data-duration={400}
          data-easing="ease"
          data-easing2="ease"
          role="banner"
          className="navbar-2 w-nav"
        >
          <div className="text-block-4 overview">Overview</div>
          <div className="container-2 w-container">
            <nav role="navigation" className="blog-menu w-nav-menu">
              <a
                href="w4#challenge"
                className="nav-link w-nav-link"
              >
                Challenge
              </a>
              <a
                href="w4#goals"
                className="nav-link w-nav-link"
              >
                Goals
              </a>
              <a
                href="w4#solutuons"
                className="nav-link w-nav-link"
              >
                Solution
              </a>
              <a
                href="w4#results"
                className="nav-link w-nav-link"
              >
                Results
              </a>
              <div className="wrapper dfdf" />
              <p className="paragraph-18">Additional Context</p>
              <a
                href="w4#approuch"
                className="nav-link w-nav-link"
              >
                Approach
              </a>
              <a
                href="w4#xfn-collaboration"
                className="nav-link w-nav-link"
              >
                Xfn Collaboration
              </a>
              <a
                href="w4#hurdles"
                className="nav-link w-nav-link"
              >
                Hurdles
              </a>
              <a
                href="w4#Education"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">Education</strong>
              </a>
              <a
                href="w4#components"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">Components library</strong>
              </a>
              <a
                href="w4#icons"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">Iconography</strong>
              </a>
              <a
                href="w4#Badges"
                id="Badges"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">Badges and Trophies</strong>
              </a>
              <a
                href="w4#future-steps"
                className="nav-link w-nav-link"
              >
                Future steps
              </a>
              <a
                href="w4#reflections"
                className="nav-link w-nav-link"
              >
                Reflection
              </a>
            </nav>
          </div>
        </div>
      </div>
      <div className="rigth-contt">
        <div id="challenge" className="challenge">
          <div className="w-richtext">
            <p>
              <strong>The Challenge</strong>
            </p>
            <h4>
              <strong>Resolving Fragmented Digital Aesthetics</strong>
            </h4>
            <p>
              The Orka Technology Group's diverse portfolio, comprising Orka
              Works, Orka Check, and Orka Pay, had long grappled with the
              dissonance of inconsistent design elements across mobile and web
              platforms. The tribulations encompassed:
            </p>
            <p>
              <strong>Inconsistency:</strong> A plethora of UI kits had fostered
              confusion among users and fragmented the brand identity.
              <strong>‍</strong>
            </p>
            <p>
              <strong>Inefficiency:</strong> Redundancies impeded productivity,
              shackling our teams' creative potential.
            </p>
            <p>
              <strong>Accessibility Gaps:</strong> A mosaic of UI components had
              inadvertently created accessibility challenges for users with
              distinct needs.
            </p>
            <p>‍</p>
            <h4>
              <strong>Understanding the Collective Landscape</strong>
            </h4>
            <p>
              <strong>‍</strong>XFnet (Cross Functional Network) collaboration,
              I worked closely with stakeholders from various departments,
              gaining valuable insights into each team's unique needs and the
              distinctive functionalities of each product.
            </p>
          </div>
          <div className="top--flexx">
            <div id="w-node-_25dcfc01-c9a4-cc43-d4b9-dcd556174e8c-9d5f6c85">
              <img
                alt=""
                loading="lazy"
                src="the-orka-design-system"
                className="w-dyn-bind-empty"
              />
            </div>
            <div id="w-node-a9b9a57b-d795-cb08-fdb0-c6beabebd356-9d5f6c85">
              <img
                alt=""
                loading="lazy"
                src="the-orka-design-system"
                className="w-dyn-bind-empty"
              />
            </div>
          </div>
        </div>
        <div id="goals" className="challenge _2">
          <div className="w-richtext">
            <p>Forging a Unified Design Landscape</p>
            <h4>
              <strong>Forging a Unified Design Landscape</strong>
            </h4>
            <ul role="list">
              <li>
                <strong>Unifying User Experiences:</strong> By obliterating the
                discrepancies, our goal was to furnish users with a seamless
                cross-platform journey.
              </li>
              <li>
                <strong>Elevating Design Efficiency:</strong> Our aim was to
                sculpt a reusable system, optimizing workflow efficiencies.
              </li>
              <li>
                <strong>Strengthening Brand Consistency:</strong> Through a
                consistent visual language, we endeavored to fortify brand
                recognition.
              </li>
              <li>
                <strong>Enhancing Accessibility:</strong> Our commitment to
                inclusivity dictated that every component we forged would align
                with the highest accessibility standards.
              </li>
            </ul>
            <p>‍</p>
          </div>
        </div>
        <div id="solutuons" className="challenge _2">
          <div className="w-richtext">
            <p>Solution</p>
            <h4>
              <strong>Navigating the Design Journey: A Prudent Odyssey</strong>
            </h4>
            <p>
              <strong>‍</strong>Our expedition towards the Orka Design System's
              harmonious symphony required meticulous deliberation of every
              design choice. Rather than imposing a monolithic vision, we sought
              to navigate the existing digital landscape, honoring each unique
              aspect (apps and platforms) while constructing bridges of
              uniformity and ease for our users.
            </p>
            <p>
              <strong>Prioritizing the Core:</strong> A close examination of
              user research and pain points spotlighted the need for
              <strong>standardization in core elements</strong> such as buttons,
              forms, and navigation. These "core atoms" garnered our focused
              attention, facilitating a consistent user language across diverse
              platforms.
            </p>
            <p>
              <strong>Embracing Flexibility:</strong> Recognizing the
              distinctive attributes of each aspect was essential. We eschewed
              rigid uniformity, opting for component libraries replete with{" "}
              <strong>customization options</strong>, catering to specific
              platform idiosyncrasies. This harmonious balance between
              uniformity and flexibility forged a cohesive experience while
              cherishing individuality.
            </p>
            <p>
              <strong>Accessibility as our North Star:</strong> Inclusivity
              wasn't an afterthought; it was ingrained into our design system's
              very essence. The bedrock of our decisions lay in{" "}
              <strong>accessibility audits</strong> and rigorous user testing,
              ensuring that our interfaces catered to a diverse audience. This
              unwavering commitment to inclusivity not only enriched the user
              experience but also broadened Orka's reach.
            </p>
            <p>
              <strong>Collaboration, the Guiding Constellation:</strong> The
              pursuit of a successful design system was no solo expedition.{" "}
              <strong>Collaboration with cross-functional teams</strong> (XFnet
              teams) was indispensable. Developers contributed insights into
              technical feasibility, product managers ensured strategic
              alignment, and customer support grounded our design in real-world
              user feedback. This collective approach ensured that our system
              not only exuded beauty but also upheld practicality and
              user-centricity.
            </p>
            <p>
              <strong>5. Phased Rollout, the Steady Course:</strong> Rather than
              an abrupt upheaval, we adopted a phased rollout strategy. Initial
              implementation of core components allowed us to{" "}
              <strong>garner user feedback</strong> and refine iteratively
              before undertaking more intricate functionalities. This gradual
              approach minimized disruptions and facilitated a smooth transition
              for both users and teams.
            </p>
            <p>‍</p>
            <h4>
              <strong>Empowering Resources for Design Excellence</strong>‍
            </h4>
            <p>
              To empower our teams and users, we meticulously crafted an array
              of resources that cater to diverse learning styles and abilities:
              <br />‍
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "4622pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659284ef6f4603777ad57361_Group%2022732.png"
                  alt=""
                />
              </div>
            </figure>
            <p />
            <p>
              <strong>1. Core Principles:</strong>
            </p>
            <p>
              <strong>Quick Reference Guide:</strong> A one-page document or
              pocket-sized booklet summarizing the key design principles, brand
              identity, and overall vision. Ideal for a quick refresh or
              on-the-go reference.
            </p>
            <p>
              <strong>Foundations Figbook:</strong> A deeper dive into the
              rationale and implementation of the design system's core elements,
              best practices, and accessibility standards. Geared towards
              designers, developers, and product managers.
            </p>
            <p>
              <strong>2. Building Blocks:</strong>
            </p>
            <p>
              <strong>Component Library Catalog:</strong> A visual and
              interactive guide showcasing each component, its variations, and
              usage examples. Great for designers and developers looking for
              specific elements and code snippets.
            </p>
            <p>
              <strong>Pattern Playbook:</strong> A collection of pre-designed UI
              patterns with customization options and best practices for
              implementation. Useful for both designers and product managers
              building consistent and efficient interfaces.
            </p>
            <p>
              <strong>3. Voice and Experience:</strong>
            </p>
            <p>
              <strong>Brand Tone &amp; Style Guide:</strong> A comprehensive
              guide defining Orka's voice across different channels, ensuring
              consistent communication and user experience. Suitable for content
              creators, marketers, and customer support teams.
            </p>
            <p>
              <strong>Accessibility Figbook:</strong> A detailed resource
              outlining accessibility standards, testing methods, and best
              practices for inclusive design. Valuable for designers,
              developers, and anyone involved in creating digital experiences.
            </p>
            <p>
              <strong>4. Living System:</strong>
            </p>
            <p>
              <strong>Release Notes &amp; Updates:</strong> A dedicated space
              for tracking changes, new features, and upcoming developments
              within the design system. Accessible to everyone for staying
              informed and adapting their work.
            </p>
            <p>
              <strong>Community Forum &amp; Resources:</strong> A collaborative
              platform for users to share feedback, ask questions, and
              contribute to the ongoing evolution of the design system. This
              open forum fosters ownership and continuous improvement.
            </p>
          </div>
        </div>
        <div id="results" className="challenge _2">
          <div className="w-richtext">
            <p>Results</p>
            <h4>Our efforts yielded tangible results</h4>
            <p>
              ‍<strong>Design Time Reduction:</strong> Streamlined workflows led
              to a 35% decrease in design time.
            </p>
            <p>
              <strong>User Satisfaction Increase:</strong> Improved interfaces
              raised user satisfaction by 20%.
            </p>
            <p>
              <strong>Brand Recognition Growth:</strong> Achieved a 15% increase
              in brand recognition.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: 1814 }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6593284b2ab5244b61efad93_Screenshot%202024-01-01%20at%2021.01.49.png"
                  alt=""
                />
              </div>
              <figcaption>Before</figcaption>
            </figure>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: 3650 }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659328c787e7a98e27268694_After.png"
                  alt=""
                />
              </div>
              <figcaption>After</figcaption>
            </figure>
            <p>‍</p>
            <p>‍</p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: 2018 }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6593297acd176c4b0f2d32c6_Screenshot%202023-06-26%20at%2016.27%201.png"
                  alt=""
                />
              </div>
              <figcaption>Before</figcaption>
            </figure>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: 9612 }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6593298c20a04daa113d0e34_Group%2022829.png"
                  alt=""
                />
              </div>
              <figcaption>Afer</figcaption>
            </figure>
          </div>
        </div>
        <div className="wrapper" />
        <div id="approuch" className="approuch">
          <div className="w-richtext">
            <p>Approach</p>
            <h4>
              <strong>
                My approach followed a structured path to success:
              </strong>
            </h4>
            <p>
              <strong>Empathy &amp; Analysis:</strong> Extensive user research
              and testing unveiled pain points and guided our design decisions.
            </p>
            <p>
              <strong>Collaboration &amp; Communication:</strong> Engaging
              stakeholders from various departments was pivotal in gaining
              buy-in and fostering understanding.
            </p>
            <p>
              <strong>Flexibility &amp; Adaptation:</strong> Balancing
              standardisation with customisation was key to accommodating the
              unique features of each product.
            </p>
            <p>
              <strong>Prioritisation &amp; Phasing:</strong> We implemented the
              design system in carefully planned stages to ensure a smooth
              integration process
              <br />
              <br />‍
            </p>
            <figure className="w-richtext-align-center w-richtext-figure-type-image">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659c363e525d39f46d58e491_Screenshot%202024-01-08%20at%2017.50.30.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="xfn-collaboration" className="approuch _2">
          <div className="w-richtext">
            <p>Xfn Collaboration</p>
            <h4>
              <strong>
                I worked closely with stakeholders from various departments,
                gaining invaluable insights into the specific needs and concerns
                of each team, as well as a deep understanding of the unique
                functionalities of each product.
              </strong>
            </h4>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65921afa50e3623a104ac365_Xfn%20Collaboration.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="hurdles" className="approuch _2">
          <div className="w-richtext">
            <p>Hurdles Conquered</p>
            <h4>
              <strong>
                My journey was not without its share of challenges
              </strong>
            </h4>
            <p>
              We encountered resistance to change, addressed technical
              limitations, and had to manage user expectations. However, these
              challenges were not stumbling blocks but stepping stones for
              learning and adaptation.
              <br />
              <br />‍
            </p>
            <figure
              style={{ maxWidth: 1558 }}
              className="w-richtext-align-fullwidth w-richtext-figure-type-image"
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659c363b4c34115a59391d3b_Screenshot%202024-01-08%20at%2017.51.03.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Education" className="approuch _2">
          <div className="w-richtext">
            <p>Educating</p>
            <h4>
              <strong>
                From Design to Deployment: Educating the Orka Crew
              </strong>
            </h4>
            <p>
              To ensure the seamless adoption of the Orka Design System, we
              implemented interactive workshops, provided comprehensive
              documentation, and established dedicated support channels, making
              the onboarding and transition process smooth and efficient.
            </p>
          </div>
          <div id="components" className="w-richtext">
            <p>Components Library</p>
            <p>‍</p>
            <figure
              style={{ maxWidth: 11967 }}
              className="w-richtext-align-fullwidth w-richtext-figure-type-image"
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659c3a03c4c2a52e54d9b5cf_Group%2022480.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
          <div id="icons" className="w-richtext">
            <p>Iconography</p>
            <p>‍</p>
            <figure
              style={{ maxWidth: 1643 }}
              className="w-richtext-align-fullwidth w-richtext-figure-type-image"
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659c3a13d669f4971cb0b412_Group%2022h829.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
            <p />
          </div>
          <div className="w-richtext">
            <p>Badges and Trophies</p>
            <p>‍</p>
            <figure
              style={{ maxWidth: 13627 }}
              className="w-richtext-align-fullwidth w-richtext-figure-type-image"
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659c3a4f73e7c216012e170a_Group%202.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="future-steps" className="approuch _2">
          <div className="w-richtext">
            <p>Future steps</p>
            <h4>
              <strong>
                My journey doesn't end here. We have ambitious plans for the
                future, including:
              </strong>
            </h4>
            <p>
              <strong>Continuous Iteration:</strong> Regular updates based on
              user feedback to ensure the design system remains responsive to
              evolving needs.
            </p>
            <p>
              <strong>Community Engagement:</strong> Sharing our methodologies
              and best practices with the design community to foster collective
              growth.
            </p>
            <p>
              <strong>Expansion of Scope:</strong> Integrating the design system
              with other internal tools, further enhancing its utility and
              reach.
            </p>
          </div>
        </div>
        <div id="reflections" className="approuch _2">
          <div className="w-richtext">
            <p>Reflection</p>
            <h4>
              <strong>
                This project transcended the realm of design; it was an odyssey
                that deepened our understanding of digital ecosystems and the
                transformative potential of cohesive design.
              </strong>
            </h4>
            <p>
              It challenged our problem-solving skills and enriched our
              appreciation for collaborative innovation. The success of the Orka
              Design System stands as a testament to the significance of
              user-centered design, strategic planning, and the fusion of
              technology and creativity in today's digital landscape. As we look
              back on this journey, it's evident that we've not only created a
              design system but also shaped a future where design is not an
              afterthought but a cornerstone of product development and user
              engagement. This journey with Orka Technology Group has been an
              invaluable experience, shaping our commitment to shaping a future
              where design is not just an afterthought but a cornerstone of
              product development and user engagement.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "3028pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659289b840d0a805b1f310a8_Living%20System.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<Footer />

    </div>
  )
}

export default W4