import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Partials from '../components/Partials'

function Home() {
  return (
    <div>
        <Header />
            <section className="section-2">
      <div className="page-padding home">
        <h1 className="hero-heading">Strategic & AI-Driven Product Designer</h1>
        <p className="paragraph-33">I'm a dynamic UX Leader based in Lagos, Nigeria, 
          with over 9 years of experience steering UX strategy and driving innovation. 
          I specialize in translating user needs into designs that fuel business success. 
          My approach combines strategic, data-informed, and user-focused methodologies to craft impactful experiences that redefine business goals. 
          Beyond design, I cultivate a vibrant team dynamic, adeptly handling complex tech challenges with humor, creating an engaging work environment.
        </p>
        <div className="wrapper"></div>
        <div data-current="Tab 1" data-easing="ease" data-duration-in="300" data-duration-out="100" className="tabs w-tabs">
          <div className="tabs-menu w-tab-menu">
            <a data-w-tab="Tab 2" id="w-node-_19e56dd2-32f1-22d5-06f6-3ab1cce60111-638315dd" className="tab-btn sds w-inline-block w-tab-link"
              ><div className="text-block-5">B2C</div></a>
              <a
              data-w-tab="Tab 3"
              muti-sel="select all"
              id="w-node-_19e56dd2-32f1-22d5-06f6-3ab1cce60114-638315dd"
              className="tab-btn w-inline-block w-tab-link"
              ><div>B2B</div></a
            ><a data-w-tab="Tab 5" id="w-node-abf360c0-213c-c5dd-79df-08790edad97c-638315dd" className="tab-btn w-inline-block w-tab-link"
              ><div>Mobile</div></a
            ><a data-w-tab="Tab 6" id="w-node-_0199ce19-0370-7a33-703f-3cea9de29de2-638315dd" className="tab-btn w-inline-block w-tab-link"
              ><div>Web</div></a
            ><a
              data-w-tab="Tab 7"
              id="w-node-_6483e755-1ffd-e8ad-de72-4cd336f1a946-638315dd"
              className="tab-btn sdsds w-inline-block w-tab-link">
            <div>Design system</div></a>
            <a data-w-tab="Tab 8" className="tab-btn sdsds w-inline-block w-tab-link"><div>AI</div></a>
          </div>
          <div className="tabs-content w-tab-content">
            <div data-w-tab="Tab 1" className="main w-tab-pane w--tab-active">
              <div className="first-all-cont lkkk">
                <a href="/w1" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2C / Mobile</div>
                  <h5 className="card-headingg">Empowering shift workers with Enhanced Financial Visibility and Predictability</h5>
                  <p className="card-sub-para">
                    Leading Orka Pay&#x27;s growth from a simple wage access service to an advanced financial empowerment tool
                  </p></a
                ><a href="/w2" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804.png"
                    loading="lazy"
                    className="card-img" />
                  <div className="card-slug">B2B / B2C / Mobile / Web</div>
                  <h5 className="card-headingg">Navigating UK Home Office Right to Work Policy Changes - Orka Group</h5>
                  <p className="card-sub-para">
                    In Sep &#x27;22, Home Office updated guidelines. I integrated them into Orka Group apps, ensuring audit success.<br /></p></a
                ><a
                  href="/w3"
                  className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726.png"
                    loading="lazy"
                    className="card-img" />
                  <div className="card-slug">B2B / B2C / Mobile / Web</div>
                  <h5 className="card-headingg">
                    Orka Works scale-up initiative to accommodate the evolving needs of workers and the job market
                  </h5>
                  <p className="card-sub-para">
                    Orka Work, a dynamic job platform, recently experienced a significant surge in user registrations, signalling a need for
                    strategic scaling. <br /></p></a
                ><a href="/w4" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">Design System / Branding</div>
                  <h5 className="card-headingg">The Orka Design System: Bridging Platforms, Uniting Experiences</h5>
                  <p className="card-sub-para">
                    Procore is the world’s #1 most-used construction management software. I was the first to redesign the 2-year, #1
                    customer-requested feature for Field Financials suite.
                  </p></a
                ><a href="/w5" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png        1276w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2B / B2C / Mobile / Web / AI / ML</div>
                  <h5 className="card-headingg">Revolutionising surveillance!<br /></h5>
                  <p className="card-sub-para">
                    Sight uses AI and computer vision to empower CCTV cameras for real-time insights and intelligent alerts.
                  </p></a
                ><a href="/w6" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2C / Mobile / Design System / AI</div>
                  <h5 className="card-headingg">A Guided Wellness Journal</h5>
                  <p className="card-sub-para">
                    While we use a hundred of apps to kill time. It&#x27;s  a little effort to save lives on time and ensure a healthy
                    lifestyle.
                  </p></a
                >
              </div>
            </div>
            <div data-w-tab="Tab 2" className="w-tab-pane">
              <div className="first-all-cont hjgjh">
                <a href="/w1" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2C / Mobile</div>
                  <h5 className="card-headingg">Empowering shift workers with Enhanced Financial Visibility and Predictability</h5>
                  <p className="card-sub-para">
                    Leading Orka Pay&#x27;s growth from a simple wage access service to an advanced financial empowerment tool
                  </p></a
                ><a href="w2/navigating-uk-home-office-rtw-policy-changes-orka" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804.png"
                    loading="lazy"
                    className="card-img" />
                  <div className="card-slug">B2B / B2C / Mobile / Web</div>
                  <h5 className="card-headingg">
                    <strong className="bold-text-3">Navigating UK Home Office Right to Work Policy Changes - Orka Group</strong>
                  </h5>
                  <p className="card-sub-para">
                    In Sep &#x27;22, Home Office updated guidelines. I integrated them into Orka Group apps, ensuring audit success.<br /></p></a
                ><a
                  href="/w3"
                  className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726.png"
                    loading="lazy"
                    className="card-img" />
                  <div className="card-slug">B2B / B2C / Mobile / Web</div>
                  <h5 className="card-headingg">
                    Orka Works scale-up initiative to accommodate the evolving needs of workers and the job market
                  </h5>
                  <p className="card-sub-para">
                    Orka Work, a dynamic job platform, recently experienced a significant surge in user registrations, signalling a need for
                    strategic scaling. <br /></p></a
                ><a href="/w5" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png        1276w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2B / B2C / Mobile / Web / AI / ML</div>
                  <h5 className="card-headingg">Revolutionising surveillance!<br /></h5>
                  <p className="card-sub-para">
                    Sight uses AI and computer vision to empower CCTV cameras for real-time insights and intelligent alerts.
                  </p></a
                ><a href="/w6" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2C / Mobile / Design System / AI</div>
                  <h5 className="card-headingg">A Guided Wellness Journal</h5>
                  <p className="card-sub-para">
                    While we use a hundred of apps to kill time. It&#x27;s  a little effort to save lives on time and ensure a healthy
                    lifestyle.
                  </p></a
                >
              </div>
            </div>
            <div data-w-tab="Tab 3" className="w-tab-pane">
              <div className="first-all-cont tb-space">
                <a href="w2/navigating-uk-home-office-rtw-policy-changes-orka" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804.png"
                    loading="lazy"
                    className="card-img" />
                  <div className="card-slug">B2B / B2C / Mobile / Web</div>
                  <h5 className="card-headingg">
                    <strong className="bold-text-3">Navigating UK Home Office Right to Work Policy Changes - Orka Group</strong>
                  </h5>
                  <p className="card-sub-para">
                    In Sep &#x27;22, Home Office updated guidelines. I integrated them into Orka Group apps, ensuring audit success.<br /></p></a
                ><a
                  href="/w3"
                  className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726.png"
                    loading="lazy"
                    className="card-img" />
                  <div className="card-slug">B2B / B2C / Mobile / Web</div>
                  <h5 className="card-headingg">
                    Orka Works scale-up initiative to accommodate the evolving needs of workers and the job market
                  </h5>
                  <p className="card-sub-para">
                    Orka Work, a dynamic job platform, recently experienced a significant surge in user registrations, signalling a need for
                    strategic scaling. <br /></p></a
                ><a href="/w5" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png        1276w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2B / B2C / Mobile / Web / AI / ML</div>
                  <h5 className="card-headingg">Revolutionising surveillance!<br /></h5>
                  <p className="card-sub-para">
                    Sight uses AI and computer vision to empower CCTV cameras for real-time insights and intelligent alerts.
                  </p></a
                >
              </div>
            </div>
            <div data-w-tab="Tab 5" className="w-tab-pane">
              <div className="first-all-cont tb-space">
                <a href="http://wahabzahid.webflow.io/works/empowering-shift-workers" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a64464ea3ca8a16341b_Group%20806.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2C / Mobile</div>
                  <h5 className="card-headingg">Empowering shift workers with Enhanced Financial Visibility and Predictability</h5>
                  <p className="card-sub-para">
                    Leading Orka Pay&#x27;s growth from a simple wage access service to an advanced financial empowerment tool
                  </p></a
                ><a
                  href="http://wahabzahid.webflow.io/works/navigating-uk-home-office-rtw-policy-changes-orka"
                  className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804.png"
                    loading="lazy"
                    className="card-img" />
                  <div className="card-slug">B2B / B2C / Mobile / Web</div>
                  <h5 className="card-headingg">
                    <strong className="bold-text-3">Navigating UK Home Office Right to Work Policy Changes - Orka Group</strong>
                  </h5>
                  <p className="card-sub-para">
                    In Sep &#x27;22, Home Office updated guidelines. I integrated them into Orka Group apps, ensuring audit success.<br /></p></a
                ><a href="/w6" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2C / Mobile / Design System / AI</div>
                  <h5 className="card-headingg">A Guided Wellness Journal</h5>
                  <p className="card-sub-para">
                    While we use a hundred of apps to kill time. It&#x27;s  a little effort to save lives on time and ensure a healthy
                    lifestyle.
                  </p></a
                ><a href="/w5" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png        1276w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2B / B2C / Mobile / Web / AI / ML</div>
                  <h5 className="card-headingg">Revolutionising surveillance!<br /></h5>
                  <p className="card-sub-para">
                    Sight uses AI and computer vision to empower CCTV cameras for real-time insights and intelligent alerts.
                  </p></a
                ><a
                  href="/w3"
                  className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726.png"
                    loading="lazy"
                    className="card-img" />
                  <div className="card-slug">B2B / B2C / Mobile / Web</div>
                  <h5 className="card-headingg">
                    Orka Works scale-up initiative to accommodate the evolving needs of workers and the job market
                  </h5>
                  <p className="card-sub-para">
                    Orka Work, a dynamic job platform, recently experienced a significant surge in user registrations, signalling a need for
                    strategic scaling. <br /></p
                ></a>
              </div>
            </div>
            <div data-w-tab="Tab 6" className="w-tab-pane">
              <div className="first-all-cont tb-space">
                <a href="w2/navigating-uk-home-office-rtw-policy-changes-orka" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592575c9b7d35320bd615bc_Group%20804.png"
                    loading="lazy"
                    className="card-img" />
                  <div className="card-slug">B2B / B2C / Mobile / Web</div>
                  <h5 className="card-headingg">
                    <strong className="bold-text-3">Navigating UK Home Office Right to Work Policy Changes - Orka Group</strong>
                  </h5>
                  <p className="card-sub-para">
                    In Sep &#x27;22, Home Office updated guidelines. I integrated them into Orka Group apps, ensuring audit success.<br /></p></a
                ><a
                  href="/w3"
                  className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/659257653a320cb81c38617d_Group%2022726.png"
                    loading="lazy"
                    className="card-img" />
                  <div className="card-slug">B2B / B2C / Mobile / Web</div>
                  <h5 className="card-headingg">
                    Orka Works scale-up initiative to accommodate the evolving needs of workers and the job market
                  </h5>
                  <p className="card-sub-para">
                    Orka Work, a dynamic job platform, recently experienced a significant surge in user registrations, signalling a need for
                    strategic scaling. <br /></p></a
                ><a href="/w5" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png        1276w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2B / B2C / Mobile / Web / AI / ML</div>
                  <h5 className="card-headingg">Revolutionising surveillance!<br /></h5>
                  <p className="card-sub-para">
                    Sight uses AI and computer vision to empower CCTV cameras for real-time insights and intelligent alerts.
                  </p></a
                >
              </div>
            </div>
            <div data-w-tab="Tab 7" className="w-tab-pane">
              <div className="first-all-cont tb-space">
                <a href="/w4" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/658e2a6561cdc35434172235_Group%20788.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">Design System / Branding</div>
                  <h5 className="card-headingg">The Orka Design System: Bridging Platforms, Uniting Experiences</h5>
                  <p className="card-sub-para">
                    Procore is the world’s #1 most-used construction management software. I was the first to redesign the 2-year, #1
                    customer-requested feature for Field Financials suite.
                  </p></a
                ><a href="/w6" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2C / Mobile / Design System / AI</div>
                  <h5 className="card-headingg">A Guided Wellness Journal</h5>
                  <p className="card-sub-para">
                    While we use a hundred of apps to kill time. It&#x27;s  a little effort to save lives on time and ensure a healthy
                    lifestyle.
                  </p></a
                >
                <div className="con-item"></div>
              </div>
            </div>
            <div data-w-tab="Tab 8" className="w-tab-pane">
              <div className="first-all-cont">
                <a href="/w5" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png        1276w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c1b2e84d95473baf5ccd_Group%20802.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2B / B2C / Mobile / Web / AI / ML</div>
                  <h5 className="card-headingg">Revolutionising surveillance!<br /></h5>
                  <p className="card-sub-para">
                    Sight uses AI and computer vision to empower CCTV cameras for real-time insights and intelligent alerts.
                  </p></a
                ><a href="/w6" className="con-item w-inline-block"
                  ><img
                    sizes="(max-width: 479px) 100vw, (max-width: 767px) 87vw, (max-width: 991px) 42vw, (max-width: 1279px) 43vw, 572px"
                    srcset="
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-500.png   500w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-800.png   800w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-1080.png 1080w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-1600.png 1600w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803-p-2000.png 2000w,
                      https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803.png        2552w
                    "
                    alt=""
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6592c3c99b7d35320b132309_Group%20803.png"
                    loading="lazy"
                    className="card-img"
                  />
                  <div className="card-slug">B2C / Mobile / Design System / AI</div>
                  <h5 className="card-headingg">A Guided Wellness Journal</h5>
                  <p className="card-sub-para">
                    While we use a hundred of apps to kill time. It&#x27;s  a little effort to save lives on time and ensure a healthy
                    lifestyle.
                  </p></a
                >
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <Footer />


    </div>
  )
}

export default Home