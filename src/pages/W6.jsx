import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'

function W6() {
  return (
    <div>
      <Header />

      <section id="Research" className="blog-main-section">
  <div className="page-padding">
    <img
      alt=""
      loading="lazy"
      width={49}
      src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933c1ec76e313ce2ed59db_6592caf4e2cae00748c562b6_Icon.png"
      className="blog-tag"
    />
    <h1 className="blog-main-heading">
      Ensuring Wellness - Making health and fitness a habit.
    </h1>
    <p>
      The core intention of creating this app is to make health a priority.
      While we use a hundred of apps to kill time. It's a little effort to save
      lives on time and ensure a healthy lifestyle. This project addresses the
      problem of shortage of applications of focusing on a person’s health with
      daily activity tracking, reminders for medical prescriptions and having a
      hospital account in one place. This data will then help in the early
      diagnosis of potential medical conditions, as well as assist scientific
      researchers understanding of existing medical conditions through the
      participation in medical research that this application offers.
    </p>
    <div className="_3rd-role-others">
      <div id="w-node-f9e93611-13cc-ea6c-5ed8-9b705428669c-ec237d7e">
        <div>
          <strong>Role</strong>
        </div>
        <div>Lead Product Designer</div>
      </div>
      <div id="w-node-eb604632-9f96-6131-6ee7-837c79e494e4-ec237d7e">
        <div>
          <strong>Timeline</strong>
        </div>
        <div>7 months, from Mar 2020 to Dec 2020</div>
      </div>
      <div id="w-node-_33b55f7d-360b-7c21-93d4-055b982e0941-ec237d7e">
        <div>
          <strong>Core Responsibilities</strong>
        </div>
        <div>
          Project Assessment, Competitor Assessment, Low-fidelity user interface
          mock-ups, Interaction Design, High-Fidelity user interface mock-ups,
          Usable Prototype
        </div>
      </div>
    </div>
  </div>
  <div className="top-img-2">
    <img
      alt=""
      loading="lazy"
      id="w-node-_9418f6db-32f6-bfa2-5ba8-9075e5375637-ec237d7e"
      src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933c1ec76e313ce2ed59df_6592cb1eb67afefeece23972_Cover.png"
    />
    <img
      alt=""
      loading="lazy"
      width="NaN"
      id="w-node-_1384060b-ead8-9dc7-83c5-ab73dffa0b7b-ec237d7e"
      src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933c1ec76e313ce2ed59e3_6592cb98b5aac8a549ac75c3_Cover.png"
    />
  </div>
  <div className="page-padding">
    <div className="all-cont">
      <div className="left-menu">
        <div
          data-animation="default"
          data-collapse="medium"
          data-duration={400}
          data-easing="ease"
          data-easing2="ease"
          role="banner"
          className="navbar-2 w-nav"
        >
          <div className="text-block-4 overview">Figuring out</div>
          <div className="container-2 w-container">
            <nav role="navigation" className="blog-menu w-nav-menu">
              <a
                href="w6#Design-Process"
                className="nav-link w-nav-link"
              >
                Design Process
              </a>
              <a
                href="w6#research"
                className="nav-link w-nav-link"
              >
                Research
              </a>
              <a
                href="w6#research"
                className="nav-link w-nav-link"
              >
                Surveys
              </a>
              <a
                href="w6#problem"
                className="nav-link w-nav-link"
              >
                Problems
              </a>
              <a
                href="w6#goals-and-objective"
                className="nav-link w-nav-link"
              >
                Goals and Objectives
              </a>
              <a
                href="w6#coref"
                className="nav-link w-nav-link"
              >
                Core Future
              </a>
              <a
                href="w6#Structure-Overview"
                className="nav-link w-nav-link"
              >
                Structure Overview
              </a>
              <a
                href="w6#Ai-powerd"
                className="nav-link w-nav-link"
              >
                AI Powered Home Screen{" "}
              </a>
              <a
                href="w6#Sehar-Health-Affinity-Map"
                className="nav-link w-nav-link"
              >
                Sehar Health Affinity Map
              </a>
              <a
                href="w6#User-Personas"
                className="nav-link w-nav-link"
              >
                User Personas
              </a>
              <a
                href="w6#Information-Hierarchy"
                className="nav-link w-nav-link"
              >
                Information Hierarchy
              </a>
              <a
                href="w6#"
                className="nav-link w-nav-link"
              >
                Xfn Collaboration
              </a>
              <div className="wrapper dfdf" />
              <p className="paragraph-18">Desiging</p>
              <a
                href="w6#Sketching-and-Ideation"
                className="nav-link w-nav-link"
              >
                Sketching and Ideation
              </a>
              <a
                href="w6#Wireframe"
                className="nav-link w-nav-link"
              >
                Wireframe
              </a>
              <a
                href="w6#Prototyping"
                className="nav-link w-nav-link"
              >
                Prototyping
              </a>
              <a
                href="w6#User-Testing"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">User Testing </strong>
              </a>
              <a
                href="w6#Mood-Board"
                className="nav-link w-nav-link"
              >
                Mood Board
              </a>
              <a
                href="w6#Style-Guide"
                className="nav-link w-nav-link"
              >
                Style Guide
              </a>
              <a
                href="w6#design-components"
                className="nav-link w-nav-link"
              >
                Design Components
              </a>
              <a
                href="w6#Daily-Activities"
                className="nav-link w-nav-link"
              >
                Daily Activities
              </a>
              <a
                href="w6#User-Education-and-Onboarding"
                className="nav-link w-nav-link"
              >
                User Education and Onboarding
              </a>
              <a
                href="w6#theconclusionss"
                className="nav-link w-nav-link"
              >
                Conclusion
              </a>
            </nav>
          </div>
        </div>
      </div>
      <div id="Design-process" className="rigth-contt">
        <div id="design-process" className="conclusions">
          <div id="Design-Process" className="w-richtext">
            <p>Design Process</p>
            <h4>
              <strong>
                Initially we used Lean UX and then Design thinking methodology
                for design and agile and scrum from over all project.
              </strong>
              <br />
              <strong>‍</strong>
            </h4>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1200pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659463c62f8fa0a33584940d_1.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="research" className="research">
          <div id="ResearCH" className="w-richtext">
            <p>Research</p>
            <p>
              Competitive analysis
              <br />
              <br />
              The research took as its starting point a review of the main
              competitors. However, there is no platform which covers all these
              functions in one application, so the three best applications from
              the different areas of the project which were:
            </p>
            <p>
              <strong>Apple HealthApp, HealthSteps and MyHeart Counts</strong>{" "}
              were chosen and their key features of UI, UX, user flow were
              studied.In addition, it was discovered that Health App from Apple
              was the closest to the Sehar Health application because it gathers
              all health-related information in one place, which was convenient
              for users and helps them to be on track with valuable information.
            </p>
          </div>
        </div>
        <div id="Core-Future" className="survey">
          <div className="w-richtext">
            <p>Survey</p>
            <h4>
              <strong>
                Over 200,000 users in Pakistan want app to see a complete
                picture of their health and fitness data.
              </strong>
            </h4>
            <p>
              Through collecting all information from the survey and existing
              research had revealed several types of users with diverse needs
              which were combined in two personas that manifest needs and weak
              points of the existing applications.
            </p>
          </div>
        </div>
        <div id="problem" className="conclusions sfsd">
          <div id="pro" className="w-richtext">
            <p>Problems</p>
            <h4>
              <strong>
                After conducting detailed user research, we have identified a
                list of key issues related to patient healthcare and fitness
                management. Here are the top five concerns:
              </strong>
            </h4>
            <p>
              <strong>Lack of Transparency: </strong>Patients and doctors often
              lack access to transparent health and fitness records. Patients
              may not maintain a comprehensive health log, and they may forget
              or be unable to carry all relevant documents when visiting a
              doctor. This lack of transparency can lead to incorrect diagnoses
              and ineffective treatments.
              <br />
              <br />
              <strong>Neglecting Follow-Up Checkups:</strong>
              Many patients do not prioritize follow-up checkups, which are
              crucial for monitoring and managing their health conditions
              effectively.
              <br />
              <br />
              <strong>Ignoring Symptoms:</strong> Patients frequently disregard
              or downplay symptoms and may postpone or avoid visiting a doctor,
              potentially worsening their health issues.
              <br />
              <br />
              <strong>Absence of Health and Fitness Guidance:</strong>
              Patients often lack proper guidance for maintaining their health
              and fitness on a daily basis. This absence of guidance makes it
              challenging for doctors to offer ongoing support and
              recommendations.
              <br />
              <br />
              <strong>Inadequate Doctor-Patient Communication:</strong>
              Effective communication between doctors and patients is often
              lacking, hindering the exchange of essential information and
              advice necessary for managing one's health effectively.These
              points succinctly capture the key challenges identified during our
              user research.
            </p>
          </div>
          <div className="top--flexx">
            <div id="w-node-_7f47648c-5f29-fe1f-8d27-9ea3f74d40c1-ec237d7e" />
            <div id="w-node-_2f57fb42-c4fa-3b8d-d86f-64b221384267-ec237d7e" />
          </div>
        </div>
        <div id="goals-and-objective" className="conclusions sfsd">
          <p>Goals and objective</p>
        </div>
        <div id="coref" className="conclusions sfsd">
          <div id="pro" className="w-richtext">
            <p>Core Features</p>
            <h4>
              <strong>
                Following several brainstorming sessions, we identified the core
                features for the app.{" "}
              </strong>
            </h4>
            <p>
              <strong>Comprehensive Health History: </strong>Users will have
              access to a hands-free, transparent doctor appointment, enabling
              healthcare providers to view fitness habits, health goals, and a
              complete patient history. This feature ensures that doctors are
              well-informed about their patients' health backgrounds.
            </p>
            <p>
              <strong>Effortless Appointment Booking: </strong>Patients can book
              live or in-person appointments quickly with just two clicks,
              ensuring a seamless and convenient scheduling process.
            </p>
            <p>
              <strong>Real-time Health Alerts: </strong>The doctor's app will
              send notifications whenever there is a red flag in the patient's
              health profile, allowing for timely intervention and monitoring of
              critical health issues.
            </p>
            <p>
              <strong>Personalised Healthcare and Lifestyle Plans: </strong>
              Leveraging <strong>AI</strong>, the app will generate customised,
              interactive healthcare plans and lifestyle recommendations for
              users. These plans will be prominently displayed on the app's home
              page, helping users make informed decisions and work towards their
              health and fitness goals.
            </p>
          </div>
          <div className="top--flexx">
            <div id="w-node-a6a25496-d036-126d-1a39-1ad89c50c364-ec237d7e" />
            <div id="w-node-a6a25496-d036-126d-1a39-1ad89c50c365-ec237d7e" />
          </div>
        </div>
        <div id="coref" className="conclusions sfsd">
          <div id="Structure-Overview" className="w-richtext">
            <p>Structure Overview</p>
            <h4>
              <strong>
                Up to this point, the project had a solid understanding of user
                needs and core functions but lacked a defined structure.
              </strong>
            </h4>
            <p>
              This stage aimed to organize research findings and create a basic
              digital flowchart, providing a clear path for users through the
              solution. The initial flow was sketched on paper and later
              digitized, guiding the development process.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1200pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659465ae286fb42d414d6e97_2.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
          <div className="top--flexx">
            <div id="w-node-_30b14bc5-fba9-5847-d0e9-e5536ef2c4f8-ec237d7e" />
            <div id="w-node-_30b14bc5-fba9-5847-d0e9-e5536ef2c4f9-ec237d7e" />
          </div>
        </div>
        <div id="Ai-powerd" className="conclusions sfsd">
          <div className="ai-powered w-richtext">
            <p>AI Powered Home Screen</p>
            <h4>
              <strong>Differentiated for each user groups</strong>
            </h4>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1728pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592c7f9e2cae00748c38725_Cover.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Sehar-Health-Affinity-Map" className="conclusions sfsd">
          <div className="w-richtext">
            <p>Affinity Map</p>
            <figure
              style={{ maxWidth: 1728 }}
              className="w-richtext-align-fullwidth w-richtext-figure-type-image"
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592c869dbb6f549d6f5d726_Group%20227.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
            <p>‍</p>
          </div>
        </div>
        <div id="Sehar-Health-Affinity-Map" className="conclusions sfsd">
          <div id="User-Personas" className="user-personas w-richtext">
            <p>User Personas</p>
            <h4>
              <strong>
                After conducting thorough research and creating an affinity map
              </strong>
            </h4>
            <p>
              We developed distinct user personas to better understand Sehar
              Health's diverse user base. To ensure a personalized experience
              that caters to the unique fitness goals of these user personas,
              the home screen has been designed to adapt based on individual
              user profiles.Here's how the home screen will differ for each
              identified user persona:
              <br />‍
            </p>
            <p>
              <strong>
                Here's how the home screen will differ for each identified user
                persona:
              </strong>
            </p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592c8a6e7771a666bef974b_Group%20228.png"
                  alt=""
                />
              </div>
            </figure>
            <p>A peak into user persona</p>
          </div>
        </div>
        <div id="Information-Hierarchy" className="conclusions sfsd">
          <div className="w-richtext">
            <p>Information Hierarchy</p>
            <h4>
              <strong>
                The information hierarchy for Wellness Journal home screen is
                carefully designed to provide a seamless and personalized
                experience for users with diverse fitness goals. We recognize
                that each user persona has unique priorities, and the
                information hierarchy reflects this understanding
              </strong>
            </h4>
            <p>
              This stage aimed to organize research findings and create a basic
              digital flowchart, providing a clear path for users through the
              solution. The initial flow was sketched on paper and later
              digitized, guiding the development process.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: 1768 }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592c918cee7ddb7134bb83b_Group%20229.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Development-Collaboration" className="conclusions sfsd">
          <div className="xfn w-richtext">
            <p>Xfn Collaboration</p>
            <p>
              The success of the Sehar Health AI-Powered Wellness Journal
              doesn't rely solely on innovative design concepts.
              <strong>‍</strong>
            </p>
            <h4>
              <strong>
                Collaboration between our design and development teams plays a
                pivotal role in bringing these concepts to life
              </strong>
            </h4>
            <p>
              Let's explore how development collaboration, following the
              information architecture and ideation phases, has contributed to
              the realization of our vision.
            </p>
            <p>
              <strong>
                Briefly recap the project's objectives, user personas, and the
                design concepts developed during the ideation phase.
              </strong>
            </p>
            <p>
              <strong>Feasibility Assessment: </strong>After a productive
              ideation phase, the development team played a vital role in
              assessing the feasibility of our design concepts.We engaged in
              discussions to understand the technical implications of our
              creative ideas.
            </p>
            <p>
              <strong>Technical Insights:</strong> Collaboration with our
              developers provided us with valuable technical insights.They
              offered suggestions on tools, frameworks, and technologies that
              could enhance the user experience and streamline the development
              process.
            </p>
            <p>
              <strong>Early Alignment: </strong>Our collaboration ensured early
              alignment between the design and development teams.This proactive
              approach prevented potential roadblocks and design changes later
              in the project, ultimately saving time and resources.
            </p>
            <p>
              <strong>Problem-Solving:</strong> Collaboration between our
              designers and developers fostered a collaborative problem-solving
              mindset.Together, we tackled technical challenges, identified
              solutions, and ensured a practical, user-friendly design.
            </p>
            <p>‍</p>
            <blockquote>Outcome:</blockquote>
            <blockquote>
              As a result of our development collaboration, we achieved a
              seamless transition from design concepts to a fully functional
              product.
            </blockquote>
            <blockquote>
              This Wellness journal was not only visually compelling but also
              technically sound and user-centric.
            </blockquote>
          </div>
        </div>
        <div className="wrapper" />
        <div
          id="Sketching-and-Ideation"
          className="sketching-and-ideation sdsd"
        >
          <div className="w-richtext">
            <p>Sketching and Ideation</p>
            <h4>
              <strong>
                Brainstorm ideas for the layout and visual design of the home
                screen. Consider how to accommodate different metrics and
                provide a seamless user experience
              </strong>
            </h4>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1568pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659468914bbdc12d4686e44a_5.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
            <h3>‍</h3>
          </div>
        </div>
        <div id="Wireframe" className="sketching-and-ideation">
          <div className="w-richtext">
            <p>Wireframes</p>
            <h4>
              <strong>
                Brainstorm ideas for the layout and visual design of the home
                screen. Consider how to accommodate different metrics and
                provide a seamless user experience
              </strong>
            </h4>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1728pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659469d0a89ef4f1e37a1dde_6.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Prototyping" className="sketching-and-ideation">
          <div className="w-richtext">
            <p>Prototyping</p>
            <h4>
              <strong>
                Developed interactive prototypes of the home screen design to
                test its functionality and usability.{" "}
              </strong>
            </h4>
            <p>
              Users were able to navigate the home screen and understand how it
              adapts to their needs.Also, on this stage of the project, the
              interactive prototype was created in order to test the usability
              and logic of the application.
              <br />
              <br />‍
            </p>
          </div>
        </div>
        <div id="User-Testing" className="sketching-and-ideation">
          <div className="w-richtext">
            <p>Testing</p>
            <h4>
              <strong>
                In our quest to provide the best possible user experience, we
                leveraged A/B testing as a crucial component of user testing for
                Sehar Health's AI-Powered Wellness Journal.
              </strong>
            </h4>
            <p>
              This approach allowed us to fine-tune and optimize the home screen
              design by comparing two or more versions with real users, gaining
              invaluable insights into what truly resonates with our diverse
              user personas. We used Userbrain to test our figma prototypes..
              <br />
              <br />
              <strong>The Challenge</strong>
            </p>
            <p>
              <strong>‍</strong>Sehar Health's user base spans a wide range of
              fitness goals and age groups.
            </p>
            <p>
              The home screen design needs to be adaptable to cater to each user
              persona.
            </p>
            <p>
              We aimed to ensure that our design decisions were based on
              data-driven insights, fostering a user-centric approach.
            </p>
            <p>‍</p>
            <p>
              <strong>Methodology</strong>
            </p>
            <p>
              <strong>‍</strong>Variant Creation: We created multiple variants
              of the home screen design, each tailored to a specific user
              persona (e.g., Weight Loss Warrior, Cardio Champion, etc.).
            </p>
            <p>
              User Segmentation: Real users from each persona group were
              identified and recruited to participate in the A/B testing.
            </p>
            <p>
              Random Assignment: Users were randomly assigned to one of the
              design variants, ensuring a fair comparison.
            </p>
            <p>
              Usage and Engagement Analysis: We closely monitored user
              interactions with the home screen, including click-through rates,
              time spent, and goal achievement.Feedback Collection: We
              encouraged users to provide feedback on their experience and
              preferences regarding the home screen design.
            </p>
            <p>‍</p>
            <p>
              <strong>Insights and Outcomes</strong>
            </p>
            <p>
              A/B testing revealed that tailoring the design to each user
              persona led to improved user satisfaction and a design that
              resonated with the diverse needs of our user base.
            </p>
            <p>‍</p>
            <p>
              <strong>The Impact</strong>
            </p>
            <p>
              Positive user feedback and higher user satisfaction. And enhanced
              goal achievement and user-centric design decisions.
            </p>
          </div>
        </div>
        <div id="Mood-Board" className="sketching-and-ideation">
          <div className="w-richtext">
            <p>Mood Board</p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592cc75d0c4400af26f4f9d_1_pdGR0qWXvs1nSOIClsbKMw.png"
                  alt=""
                />
              </div>
            </figure>
            <h3>‍</h3>
          </div>
        </div>
        <div id="Style-Guide" className="sketching-and-ideation">
          <div className="w-richtext">
            <p>Style Guide</p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: 1229 }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592cce94d6df02a5f90195b_Group%20219.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="design-components" className="design-components">
          <div className="w-richtext">
            <p>Design Components</p>
            <h4>
              <strong>
                In order to keep experience intuitive on the project, the
                consistent design system was built.{" "}
              </strong>
            </h4>
            <p>
              Atomic principles were used in order to create all components of
              the project which could be used in the future development of the
              project like websites, new sections of the application or
              printable. For the majority of elements were created different
              stages like active, inactive or disabled.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "2043pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592cd25cd176c4b0ff67898_Group%20231.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Daily-Activities" className="sketching-and-ideation">
          <div className="w-richtext">
            <p>Daily Activities</p>
            <h4>
              <strong>
                All daily activity in the application lets a user track daily
                progress and general trends over time.{" "}
              </strong>
            </h4>
            <p>
              In a few simple clicks, weight or heart rate can be added: all
              other information would be automatically transferred from a user’s
              phone.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1638pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592cd56fbcdaf04471e9afd_Group%20220.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div
          id="User-Education-and-Onboarding"
          className="sketching-and-ideation"
        >
          <div className="w-richtext">
            <p>User Education and Onboarding</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1728pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592cd8c764d512c268d196a_Group%20221.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="theconclusionss" className="conclusions sfsd">
          <div className="conclusion w-richtext">
            <p>Conclusion</p>
            <h4>
              <strong>What did I learn?</strong>
            </h4>
            <p>
              I've learned that designing the app has been both challenging and
              rewarding. Through this project, I've come to understand the
              critical importance of the research stage. It's particularly
              challenging to create a product for two distinct user groups:
              active users who use the application daily and passive users who
              analyze data and require different types of treatments. The
              project's relevance and true value for users only became evident
              after conducting thorough medical research and surveys.
            </p>
            <h4>
              <strong>What are the next steps?</strong>
            </h4>
            <p>Improve user flow</p>
            <p>Develop the product scaling plan (web version)</p>
            <p>‍</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

      <Footer />

    </div>
  )
}

export default W6