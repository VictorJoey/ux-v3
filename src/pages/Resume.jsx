import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Partials from '../components/Partials'

function Resume() {
  return (
    <div>

          
            <Header />

                <section className="section-3">
                <div className="page-padding">
                    <div className="all-page-content">
                    <div className="left--meneu asdasd">
                        <div
                        data-animation="default"
                        data-collapse="medium"
                        data-duration="400"
                        data-easing="ease"
                        data-easing2="ease"
                        role="banner"
                        className="navbar w-nav"
                        >
                        <div className="container-3 w-container">
                            <nav role="navigation" className="abouttt w-nav-menu">
                            <a href="resume#Summary" className="nav-link w-nav-link">Summary</a>
                            <a href="resume#Skiils" className="hr-link w-nav-link">Skills</a>
                            <a href="resume#Tools" className="hr-link w-nav-link">Tools</a>
                            <a href="resume#Leadership" className="hr-link w-nav-link">Leadership &amp; Collaboration</a>
                            <a href="resume#Corporate" className="hr-link w-nav-link">Corporate experience</a>
                            <a href="resume#Community" className="hr-link w-nav-link">Community Impact</a>
                            <a href="resume#Education" className="hr-link w-nav-link">Education</a>
                            </nav>
                        </div>
                        </div>
                    </div>
                    <div className="all-contt">
                        <div className="div-block-8">
                        <div><h1 className="heading">Resume</h1></div>
                        <div id="w-node-_78af5877-39f2-d7ff-2fac-fef1c8c4daad-638315e9" className="div-block-9">
                            <a
                            href="https://drive.google.com/drive/folders/1UI4dXHnKDIB-YxsmdsgLkv6OHSse3aob?usp=sharing"
                            target="_blank"
                            className="button-6 w-button"
                            >Download Resume <span className="down--icon"></span></a
                            >
                        </div>
                        </div>
                        <div id="Summary" className="why-i-design">
                        <p className="paragraph-20">Summary</p>
                        <h3 className="heading-2">Hi, I am Victor</h3>
                        <p className="paragraph-43">
                            Product Design • UI/UX/CX • Interactions • Research • Leadership • Service Design • Visual Design • Growth &amp; Strategy
                            <br /><br />Metrics-driven Product Designer with 9+ years of experience crafting impactful user experiences. My passion lies in
                            translatinguser needs into business-driven designs that not only enhance user journeys but also deliver measurable results.
                        </p>
                        <div className="two--imagesss">
                            <div className="lftt"></div>
                            <div className="righttt"></div>
                        </div>
                        </div>
                        <div id="Skiils" className="what-i-love-designing skills">
                        <p className="tsggsc">Skills</p>
                        <h1 className="heading-3">Product Design</h1>
                        <p className="paragraph-44">
                            Ul/UX Design, Interaction Design, Service Design, HCI &amp; HCD, Product Strategy, Information Architecture, Wireframes &amp;
                            Mockups, Interactive Prototyping, Design System, UX Scrum, Complex Workflows, Agile Workflow, Data Analytics, Sketching,
                        </p>
                        <h1 className="heading-3">Research</h1>
                        <p className="paragraph-44">
                            User Research &amp; Interviews, Journey Mapping, Persona Development, Field Study, A/B Testing, Usability Testing, User Surveys,
                            Focus Groups, Research Synthesis, Guerrilla Testing, Heuristic Evaluation, Affinity Diagramming, Cognitive Walkthrough, Design
                            Thinking
                        </p>
                        <h1 className="heading-3">Prioritization</h1>
                        <p className="paragraph-44">
                            RICE, Value vs. Effort, Cost of Delay, Kano Model, MoSCoW Method, Impact-Effort Matrix, Story Mapping, Opportunity Scoring, Buy
                            a Feature
                        </p>
                        <h1 className="heading-3">Front-end Development</h1>
                        <p className="paragraph-44">
                            HTML, CSS, Tailwind css, Vue.Js, React & NextJS
                        </p>
                        </div>
                        <div id="Tools" className="what-i-love-designing tools">
                        <p className="tsggsc">Tools</p>
                        <h1 className="heading-3">Prototype</h1>
                        <p className="paragraph-44">
                            Figma, Figjam, Principle, Storybook, Chromatic, Zeroheight, XD, Sketch, Moqups, Framer, Axure RP, InVision, Protopie, Webflow,
                            Mockflow, Mockplus.
                        </p>
                        <h1 className="heading-3">Adobe</h1>
                        <p className="paragraph-44">
                            Adobe Suite, Illustrator, Experience Design (XD), InDesign, After Effects (Motion Design &amp; Interface Animation), Dimension.
                        </p>
                        <h1 className="heading-3">Other</h1>
                        <p className="paragraph-44">
                            Usertesting.com, Datadog, Mode Analytics, Google Analytics, GitLab, Notion, ChatGPT, SketchUp, Procreate, Miro, Coggle.
                        </p>
                        </div>
                        <div id="Leadership" className="what-i-love-designing leadership">
                        <p className="tsggsc">Leadership &amp; Collaboration</p>
                        <h1 className="heading-3">Leadership &amp; Collaboration</h1>
                        <p className="paragraph-44">
                            Team Building, Conflict Resolution, Mentorship, Leadership in Innovation, Cross-Functional Collaboration, Organizing Workshops,
                            Facilitating Design Critique, Self Starter, Detail-Oriented, Flexible, Systematic, Communicative, Project Management.
                        </p>
                        </div>
                        <div id="Corporate" className="what-i-love-designing">
                        <p className="tsggsc">Corporate Experience</p>
                        <div>
                            <div className="job">
                            <h1 className="jobheading">Principal Product Designer</h1>
                            <p className="paragraph-44">
                                <strong>Orka Technology Group <br /></strong>Mar 2022 - Dec 2023   |   Remote   |   Manchester, UK
                            </p>
                            </div>
                            <div className="job">
                            <h1 className="jobheading">Lead Product Designer</h1>
                            <p className="paragraph-44">
                                <strong>ID Ware<br />‍</strong>Dec 2020 - <strong>‍</strong>Mar 2022   |   Remote   |   Germany
                            </p>
                            </div>
                            <div className="job">
                            <h1 className="jobheading">Senior Product Designer</h1>
                            <p className="paragraph-44">
                                <strong>VisionX<br />‍</strong>Jan – Dec 2020   |   Remote   |   USA
                            </p>
                            </div>
                            <div className="job">
                            <h1 className="jobheading">Senior Product Designer</h1>
                            <p className="paragraph-44">
                                <strong className="bold-text-4">Face44<br />‍</strong>Dec 2018 - Jan 2020   |   Remote   |   USA
                            </p>
                            </div>
                        </div>
                        </div>
                        <div id="Community" className="what-i-love-designing community">
                        <p className="tsggsc">Community Impact</p>
                        <h1 className="heading-3">Community Impact</h1>
                        <p className="paragraph-44">
                            - Designed and instructed a 12-week program for SEN Kids communities through C.S.I.N (‘22 ’23).<br />- Led a brand and site
                            overhaul for Adoption Care (‘18 ’19), reducing bounce rates by 15%.
                        </p>
                        </div>
                        <div id="Education" className="what-i-love-designing education">
                        <p className="tsggsc">Education</p>
                        <h1 className="heading-3">UX Design Fundamentals</h1>
                        <p className="paragraph-44">
                            <strong>Computer Science<br /></strong> University of Nairobi, Kenya
                        </p>
                        </div>
                        <div id="Social-Links" className="social-links">
                        <p className="paragraph-19">Testimonials</p>
                        <h1 className="heading-7 all-aboutt">What they say about me</h1>
                        <div className="testimonail-cont-row dasdcs">
                            <div
                            data-delay="4000"
                            data-animation="slide"
                            className="testimonail-slif-der w-slider"
                            data-autoplay="false"
                            data-easing="ease"
                            data-hide-arrows="false"
                            data-disable-swipe="false"
                            data-autoplay-limit="0"
                            data-nav-spacing="3"
                            data-duration="500"
                            data-infinite="true"
                            >
                            <div className="testimonial w-slider-mask">
                                <div className="slide-1 w-slide">
                                <div className="tstst-contt">
                                    <img
                                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/65899fac4ad6601ebd08027b_652d45b1e589d2c67a53e453_New%20Linkedin.png"
                                    loading="lazy"
                                    width="68"
                                    alt=""
                                    />
                                    <h5 className="test-namee">Cassie Watson</h5>
                                    <div>HEAD OF PRODUCT - ORKATE</div>
                                    <p className="test--contt">
                                    Victor is passionate about user journeys, accessibility and implementing design principles in his work- all of which
                                    he can do while creating stunning, inspired designs. He communicates incredibly well, talking you through his process,
                                    giving and receiving feedback, and articulating his designs to stakeholders at any level with the same ease. I&#x27;ve
                                    enjoyed working with Victorso much- I come away from every conversation feeling creative and motivated.
                                    </p>
                                </div>
                                </div>
                                <div className="slide-1 w-slide">
                                <div className="tstst-contt">
                                    <img
                                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/65899fac4ad6601ebd08027b_652d45b1e589d2c67a53e453_New%20Linkedin.png"
                                    loading="lazy"
                                    width="68"
                                    alt=""
                                    />
                                    <h5 className="test-namee">Client Name</h5>
                                    <p className="test--contt">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.
                                    </p>
                                </div>
                                </div>
                                <div className="slide-1 w-slide">
                                <div className="tstst-contt">
                                    <img
                                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/65899fac4ad6601ebd08027b_652d45b1e589d2c67a53e453_New%20Linkedin.png"
                                    loading="lazy"
                                    width="68"
                                    alt=""
                                    />
                                    <h5 className="test-namee">Client Name</h5>
                                    <p className="test--contt">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.
                                    </p>
                                </div>
                                </div>
                                <div className="slide-1 w-slide">
                                <div className="tstst-contt">
                                    <img
                                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/65899fac4ad6601ebd08027b_652d45b1e589d2c67a53e453_New%20Linkedin.png"
                                    loading="lazy"
                                    width="68"
                                    alt=""
                                    />
                                    <h5 className="test-namee">Client Name</h5>
                                    <p className="test--contt">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.
                                    </p>
                                </div>
                                </div>
                            </div>
                            <div className="left-arroe-sec w-slider-arrow-left"><div className="left-arrow-tes sd"></div></div>
                            <div className="right-arrow-3 w-slider-arrow-right"><div className="left-arrow-tes"></div></div>
                            <div className="slide-nav-2 w-slider-nav w-round"></div>
                            </div>
                        </div>
                        <div className="div-block-19 zsdasd"><a href="resume#">View to linkedinAll</a></div>
                        <div
                            data-delay="4000"
                            data-animation="slide"
                            className="slider-7 w-slider"
                            data-autoplay="false"
                            data-easing="ease"
                            data-hide-arrows="false"
                            data-disable-swipe="false"
                            data-autoplay-limit="0"
                            data-nav-spacing="3"
                            data-duration="500"
                            data-infinite="true"
                        >
                            <div className="mask-2 w-slider-mask">
                            <div className="slide-7 w-slide">
                                <div className="tstst-contt-2">
                                <img
                                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/65929bc60b9c2adfb32b8b78_1658919354182.jpeg"
                                    loading="lazy"
                                    width="68"
                                    alt=""
                                    className="testimial-img"
                                />
                                <p className="test--contt-2">
                                    Victor is really fun to work with, one cannot get bored in his company... so he knows how to balance the work and life
                                    aspects nicely.
                                </p>
                                <h5 className="test-namee">Qaisar Ahmad</h5>
                                <div className="text-block-10">Creative Director - Face44</div>
                                </div>
                            </div>
                            <div className="w-slide">
                                <div className="tstst-contt-2">
                                <img
                                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/65929c294c9703a83e6c2cf1_1531837011405.jpeg"
                                    loading="lazy"
                                    width="68"
                                    alt=""
                                    className="testimial-img"
                                />
                                <p className="test--contt-2">
                                    Victor is a very skilled and competent product designer and who would be a great addition to any business looking to
                                    build innovative products.
                                </p>
                                <h5 className="test-namee">James Doyle</h5>
                                <div>Co Founder and COO - Orka Technology Group</div>
                                </div>
                            </div>
                            <div className="w-slide">
                                <div className="tstst-contt-2">
                                <img
                                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6595ee07f29b394bf989f5db_1681227199449.jpeg"
                                    loading="lazy"
                                    width="68"
                                    alt=""
                                    className="testimial-img"
                                />
                                <p className="test--contt-2">
                                    Victor is passionate about user journeys, accessibility and implementing design principles in his work- all of which he
                                    can do while creating stunning, inspired designs. I&#x27;ve enjoyed working with Victorso much- I come away from every
                                    conversation feeling creative and motivated.
                                </p>
                                <h5 className="test-namee">Cassie Watson</h5>
                                <div>Head of Product - Orka Technology Group</div>
                                </div>
                            </div>
                            <div className="w-slide">
                                <div className="tstst-contt-2">
                                <img
                                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6595ee775b718c6e3091cf8a_1611076796985.jpeg"
                                    loading="lazy"
                                    width="68"
                                    alt=""
                                    className="testimial-img"
                                />
                                <p className="test--contt-2">
                                    Victor is a very skilled and competent product designer and who would be a great addition to any business looking to
                                    build innovative products.
                                </p>
                                <h5 className="test-namee">Zoe Rigley</h5>
                                <div>Head of Product at CG Hero</div>
                                </div>
                            </div>
                            <div className="w-slide">
                                <div className="tstst-contt-2">
                                <img
                                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6595ef18ec8c1772872a3348_1692343421170.jpeg"
                                    loading="lazy"
                                    width="68"
                                    alt=""
                                    className="testimial-img"
                                />
                                <p className="test--contt-2">
                                    Victor offer a harmonious blend of aesthetics and functionality, delivering a seamless user experience. His unwavering
                                    commitment to perfection, proactive problem-solving, and innovative mindset make him an excellent fit for these roles.
                                </p>
                                <h5 className="test-namee">Ross Boardman</h5>
                                <div>Fractional CTO - Orka Technology Group</div>
                                </div>
                            </div>
                            </div>
                            <div className="left-arrow-7 w-slider-arrow-left"><div className="w-icon-slider-left"></div></div>
                            <div className="right-arrow-7 w-slider-arrow-right"><div className="w-icon-slider-right"></div></div>
                            <div className="slide-nav-8 w-slider-nav w-slider-nav-invert w-round"></div>
                        </div>
                        <a href="https://www.linkedin.com/in/viktor-joseph/" target="_blank" className="link-4">View all on Linkedin</a>
                        </div>
                    </div>
                    </div>
                </div>
                </section>

                <Footer />


    </div>
  )
}

export default Resume