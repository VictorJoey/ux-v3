import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'

function W5() {
  return (
    <div>

        <>
        <Header />
  <section className="blog-main-section">
    <div className="page-padding">
      <img
        alt=""
        loading="lazy"
        width={50}
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933cbbd854e03b88d29644_65929a0c8583e71813b6e3c6_Group%252023.png"
        className="blog-tag"
      />
      <h1 className="blog-main-heading">Revolutionising surveillance!</h1>
      <p>
        Orka Work, a dynamic job platform, recently experienced a significant
        surge in user registrations, signaling a need for strategic scaling. The
        project focused on expanding into the hospitality sector and introducing
        the 'Switch' feature to facilitate easy access to a variety of job
        opportunities.
      </p>
      <div className="_3rd-role-others">
        <div id="w-node-_442f48a1-ccc9-8650-cb1b-5984ef1e9ec1-5aa562e3">
          <div>
            <strong>Role</strong>
          </div>
          <div>Lead Product Designer</div>
        </div>
        <div id="w-node-_928708cc-c323-6775-a20e-75a23cec7b56-5aa562e3">
          <div>
            <strong>Timeline</strong>
          </div>
          <div>7 months, from Mar 2019 to Dec 2020</div>
        </div>
        <div id="w-node-_69bb3f34-a5c0-0ce2-e466-34b1c5984c32-5aa562e3">
          <div>
            <strong>Core Responsibilities</strong>
          </div>
          <div>
            Collaborated closely with UI/UX designers, developers, and brand
            strategists, fostering an environment of innovation.
          </div>
        </div>
      </div>
    </div>
  </section>
  <div className="top-img-2">
    <div id="w-node-_18227864-3282-be2a-e05e-6f6da82a0a22-5aa562e3">
      <img
        alt=""
        loading="lazy"
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933cbbd854e03b88d29648_659326d52ecb4d6931e75216_Living%2520Systegm.png"
      />
    </div>
    <div id="w-node-_35ae27a7-3207-11de-a46b-f1c34072cbf4-5aa562e3">
      <img
        alt=""
        loading="lazy"
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933cbbd854e03b88d29640_6593275c7a7ca5bdd094b3a4_ui.png"
      />
    </div>
  </div>
  <div className="page-padding">
    <div className="all-cont">
      <div className="left-menu">
        <div
          data-animation="default"
          data-collapse="medium"
          data-duration={400}
          data-easing="ease"
          data-easing2="ease"
          role="banner"
          className="navbar-2 w-nav"
        >
          <div className="text-block-4 overview">Figuring out</div>
          <div className="container-2 w-container">
            <nav role="navigation" className="blog-menu w-nav-menu">
              <a
                href="w5#challenge"
                className="nav-link w-nav-link"
              >
                Challenge
              </a>
              <a
                href="w5#project-assessment"
                className="nav-link w-nav-link"
              >
                Project Assessment
              </a>
              <a
                href="w5#Design-Process"
                className="nav-link w-nav-link"
              >
                Design Process
              </a>
              <a
                href="w5#Competitive-analysis"
                className="nav-link w-nav-link"
              >
                Competitive analysis
              </a>
              <a
                href="w5#Report"
                className="nav-link w-nav-link"
              >
                Report
              </a>
              <a
                href="w5#Survey"
                className="nav-link w-nav-link"
              >
                Survey
              </a>
              <a
                href="w5#Objectives"
                className="nav-link w-nav-link"
              >
                Objectives
              </a>
              <div className="wrapper dfdf" />
              <p className="paragraph-18">Desiging</p>
              <a
                href="w5#Structure-overview"
                className="nav-link w-nav-link"
              >
                Structure overview
              </a>
              <a
                href="w5#wirframe"
                className="nav-link w-nav-link"
              >
                Wireframe
              </a>
              <a
                href="w5#Mood-Board"
                className="nav-link w-nav-link"
              >
                Mood Board
              </a>
              <a
                href="w5#style-guide"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">Style Guide</strong>
              </a>
              <a
                href="w5#Design-components"
                className="nav-link w-nav-link"
              >
                Design Components
              </a>
              <a
                href="w5#Final-Product"
                className="nav-link w-nav-link"
              >
                Final Product
              </a>
              <a
                href="w5#Conclusions"
                id="Conclusions"
                className="nav-link w-nav-link"
              >
                Conclusion
              </a>
            </nav>
          </div>
        </div>
      </div>
      <div className="rigth-contt">
        <div id="challenge" className="challenge">
          <div className="w-richtext">
            <p>
              <strong>The Challenge</strong>
            </p>
            <h4>
              <strong>
                Elevating Property Marketing through AI-Enhanced CCTV
                Surveillance.
              </strong>
            </h4>
            <p>
              This initial stage entailed in-depth exploration of the
              shortcomings inherent in traditional security camera setups,
              juxtaposed with the potential advantages stemming from the fusion
              of AI and cutting-edge computer vision technologies.
            </p>
          </div>
          <div className="top--flexx">
            <div id="w-node-_3023a009-3413-df3c-fba7-35823742c5e2-5aa562e3">
              <img
                alt=""
                loading="lazy"
                src="revolutionising-surveillance"
                className="w-dyn-bind-empty"
              />
            </div>
            <div id="w-node-c18058b3-617f-9c36-3545-5009bd9c02a3-5aa562e3">
              <img
                alt=""
                loading="lazy"
                src="revolutionising-surveillance"
                className="w-dyn-bind-empty"
              />
            </div>
          </div>
        </div>
        <div id="project-assessment" className="challenge _222">
          <div className="w-richtext">
            <p>Project Assessment</p>
            <h4>
              <strong>Competitor Assessment </strong>
            </h4>
            <h4>
              <strong>Low-fidelity user interface mock-ups</strong>
            </h4>
            <h4>
              <strong>Feasibility sync wih engineers</strong>
            </h4>
            <h4>
              <strong>Usability testing• Design System</strong>
            </h4>
            <h4>
              <strong>High-Fidelity user interface mock-ups</strong>
            </h4>
            <h4>
              <strong> Usable Prototype</strong>
            </h4>
          </div>
        </div>
        <div id="Design-Process" className="challenge _222">
          <div className="w-richtext">
            <p>Design Process</p>
            <h4>
              <strong>
                Design thinking methodology for design and agile and scrum from
                over all project.
              </strong>
            </h4>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "2400pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6594720ceb365dc2cda6d49e_1.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Competitive-analysis" className="challenge _222">
          <div className="w-richtext">
            <p>Competitive analysis</p>
            <figure className="w-richtext-align-center w-richtext-figure-type-image">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65947254d5a3d6e3427ea0ae_Group%20216.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Report" className="challenge _222">
          <div className="w-richtext">
            <p>Report</p>
            <h4>
              <strong>
                The aim of the survey was to understand user preferences,
                challenges, and expectations related to these solutions.
              </strong>
            </h4>
            <p>
              The report outlines key findings and recommendations based on the
              responses received.Methodology:The survey was conducted online
              over a period of four weeks. A diverse pool of participants from
              various industries, including security professionals, facility
              managers, and decision-makers, responded to the questionnaire. The
              survey comprised a mix of closed-ended and open-ended questions,
              allowing for both quantitative and qualitative analysis.Key
              Findings:
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1253pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6594728267c6c023ad780733_Group%20217.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Survey" className="challenge _222">
          <div className="w-richtext">
            <p>Survey</p>
            <h4>
              <strong>Recommendations</strong>
            </h4>
            <p>‍</p>
            <p>
              <strong>Customization: </strong>Companies should offer solutions
              with varying levels of customization to cater to different user
              needs and IT environments.
            </p>
            <p>
              <strong>Education:</strong> Address concerns about data privacy
              and security through clear communication and transparency about
              data handling practices.
            </p>
            <p>
              <strong>Affordability:</strong> Consider pricing strategies that
              accommodate different budget sizes, particularly for smaller
              businesses.
            </p>
            <p>
              <strong>Integration:</strong> Emphasize seamless integration
              capabilities with existing surveillance systems to ease the
              transition process.
            </p>
            <p>
              <strong>Training and Support:</strong> Offer comprehensive
              training and support resources to help users maximize the benefits
              of AI-powered solutions.
            </p>
            <p>‍</p>
            <blockquote>
              Conclusion:The survey underscores the growing awareness and
              interest in AI-powered surveillance solutions among professionals
              responsible for security and surveillance. Real-time alerts,
              accurate object detection, and ease of integration emerged as
              primary considerations. Addressing concerns related to data
              security and providing customizable options could enhance the
              adoption of these solutions across various industries. Companies
              that effectively address these needs are poised to shape the
              future of the AI-powered surveillance landscape.
            </blockquote>
          </div>
        </div>
        <div id="Objectives" className="challenge _222">
          <div className="w-richtext">
            <p>
              Objectives
              <br />
            </p>
            <ol role="list">
              <li>
                Amplifying Visual Insights:Develop an AI-driven system that
                amplifies visual insights derived from CCTV camera feeds
              </li>
              <li>
                Instantaneous Alerts for Strategic Marketing:Implement real-time
                alerts to swiftly identify high-traffic zones, facilitating
                targeted marketing campaigns.
              </li>
              <li>
                Activation of Intelligent Marketing Triggers:Create responsive
                actions triggered by identified patterns to optimize marketing
                tactics.
              </li>
              <li>
                Data-Driven Visualization for Informed Decisions:Offer
                comprehensive data visualizations that drive marketing decisions
                tailored to specific mall areas.
              </li>
              <li>
                Enhanced ROI with Precise Marketing:Augment Return on Investment
                (ROI) by channeling marketing efforts according to AI-derived
                insights.6. Personalized Customer Experiences:Leverage AI
                insights to deliver personalized experiences to mall visitors.
              </li>
              <li>
                Agile Marketing Strategy Refinement: Continuously monitor and
                recalibrate marketing strategies based on real-time intelligent
                inputs.
              </li>
            </ol>
            <p />
          </div>
        </div>
        <div id="Structure-overview" className="challenge _222">
          <div className="w-richtext">
            <p>Structure overview</p>
            <h4>
              <strong>
                Till this point, the project had a solid idea and understanding
                about users and core functions of the application but no
                definite structure and functionality.
              </strong>
            </h4>
            <p>
              The goal of this stage of the project was to organise and
              structure all the content from the research phase. Also, creating
              the basic flow forced to think about each step on the path of the
              users throughout the solution. The basic flow initially was
              created on paper and after was transferred in digital format.
            </p>
            <p>‍</p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65947344746aac9b5a9309d8_wmzznTzM%201.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="wirframe" className="challenge _222">
          <div className="w-richtext">
            <p>Wireframes</p>
            <h4>
              <strong>
                Using the structure of the user flow, the wireframes were
                designed.
              </strong>
            </h4>
            <p>
              It helped to focus on the functionality rather than on visual
              appearance. The main screen of the application is the dashboard,
              where a user could find a Reports, Insights, Stats and heatmaps,
              live and for the day.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1390pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6594738714ae05ff5ae10c5b_wireframe.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Mood-Board" className="challenge _222">
          <div className="w-richtext">
            <p>
              Moodboard
              <br />‍
            </p>
            <figure className="w-richtext-align-center w-richtext-figure-type-image">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6594739a8dc7168dcf8245b8_Mood%20Board.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="style-guide" className="challenge _222">
          <div className="w-richtext">
            <p>Style Guide</p>
            <h4>
              <strong>
                A style guide is more than a collection of fonts and colour
              </strong>
              s
            </h4>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1320pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659473c4d5a3d6e3427f8b4e_Group%20224.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Design-components" className="challenge _222">
          <div className="w-richtext">
            <p>Design System</p>
            <h4>
              <strong>
                In order to keep experience intuitive on the project, the
                consistent design system was built.{" "}
              </strong>
            </h4>
            <p>
              Atomic principles were used in order to create all components of
              the project which could be used in the future development of the
              project like websites, new sections of the application or
              printables.
              <br />
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1920pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659474019ac2ebcf6706ef06_Design%20Components.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Final-Product" className="challenge _222">
          <div className="w-richtext">
            <p>Final product</p>
            <h4>
              <strong>
                The core focus was on delivering a seamless user experience.
              </strong>
            </h4>
            <p>
              To achieve this, we adopted a minimalist and subtly illustrative
              design strategy. This approach not only facilitates effortless
              navigation but also adds an element of delight to the user's
              interaction. By combining these design principles, we ensured a
              user-friendly interface that effectively presents all data in a
              clear and organized manner.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "2162pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6594741e2f8fa0a3358e6710_Final%20Product.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1920pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65947433060c8819ab5b4781_Slide%2016_9%20-%2015.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Conclusion" className="challenge _222">
          <div className="w-richtext">
            <p>Conclution</p>
            <h4>
              <strong>What did I learn?</strong>
            </h4>
            <p>
              Concluding the design of Sight marks a rewarding and challenging
              journey. Throughout its development, I've come to recognize the
              critical significance of thorough research. Creating a solution
              that caters to both active users relying on the application daily
              and passive users who analyze intricate data presented a
              formidable task. It was only through in-depth exploration of
              surveillance technology, coupled with extensive surveys, that the
              project truly crystallized into a valuable asset, offering genuine
              utility and insight to our user base.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <Footer />
</>


    </div>
  )
}

export default W5