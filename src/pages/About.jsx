import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Partials from '../components/Partials'

function About() {
  return (
    <div>

           <Header />

            <section className="section-3">
            <div className="page-padding">
                <div className="all-page-content">
                <div className="left--meneu sdsad">
                    <div
                    data-animation="default"
                    data-collapse="medium"
                    data-duration="400"
                    data-easing="ease"
                    data-easing2="ease"
                    role="banner"
                    className="navbar w-nav"
                    >
                    <div className="container-3 w-container">
                        <nav role="navigation" className="abouttt w-nav-menu">
                        <a href="about#why-i-design" className="nav-link w-nav-link">Why I Design</a
                        ><a href="about#What-I-Love-Designing" className="hr-link w-nav-link">What I Design</a
                        ><a href="about#Personality" className="hr-link w-nav-link">Personality</a
                        ><a href="about#Family" className="hr-link w-nav-link">Family</a><a href="about#Hobbies" className="hr-link w-nav-link">Hobbies</a>
                        </nav>
                        <div className="w-nav-button"><div className="w-icon-nav-menu"></div></div>
                    </div>
                    </div>
                </div>
                <div className="all-contt">
                    <h1 className="heading">Design + Music + Speed = Me</h1>
                    <div id="why-i-design" className="why-i-design">
                    <p className="paragraph-20">Why I Design</p>
                    <h3 className="heading-2">Design is my way of making a difference.</h3>
                    <p className="paragraph-34">
                        It&#x27;s about empathy and inclusivity, values I&#x27;ve always held close. For me, the real thrill lies in unraveling complex
                        problems and turning them into user-centric solutions that really matter.
                    </p>
                    <div className="two--imagesss">
                        <div className="lftt">
                        {/* <img
                                src="../img/black-profile.png"
                                loading="lazy"
                                width="495"
                                sizes="(max-width: 479px) 100vw, (max-width: 767px) 35vw, (max-width: 991px) 34vw, (max-width: 1279px) 33vw, 429.5px"
                                alt=""
                                srcset="
                                https://assets-global.website-files.com/6589829c7efa8f55638314fd/65bef69c8d0f322a3950eb94_WahabZahid-p-500.png   500w,
                                https://assets-global.website-files.com/6589829c7efa8f55638314fd/65bef69c8d0f322a3950eb94_WahabZahid-p-800.png   800w,
                                https://assets-global.website-files.com/6589829c7efa8f55638314fd/65bef69c8d0f322a3950eb94_WahabZahid-p-1080.png 1080w,
                                https://assets-global.website-files.com/6589829c7efa8f55638314fd/65bef69c8d0f322a3950eb94_WahabZahid-p-1600.png 1600w,
                                https://assets-global.website-files.com/6589829c7efa8f55638314fd/65bef69c8d0f322a3950eb94_WahabZahid.png        1920w
                                "/> */ }
                        <img src="assets/img/black-profile.png" loading="lazy" width="495" />
                        </div>
                        <div className="righttt">
                        <img src="assets/img/bike.png" loading="lazy" width="auto" alt="" className="image-14" />
                        </div>
                    </div>
                    </div>
                    <div id="What-I-Love-Designing" className="what-i-love-designing">
                    <p className="tsggsc">What I Love Designing</p>
                    <h1 className="heading-3">
                        I&#x27;m all about creating products that pack a punch – ones that are innovative, impactful, and empowering.
                    </h1>
                    <p>
                        I get a kick out of transforming challenging user needs into designs that drive business forward. Whether it&#x27;s shaping
                        startups or steering educational initiatives, I&#x27;m there, bringing people together and making things happen.
                        <br /><br />Right now, I&#x27;m diving deep into the world of AI.At Orka, I was part of something big. We were revolutionizing
                        how the facilities management industry works, focusing on the people who keep it running: hourly workers. My job? To make their
                        lives easier through smarter hiring, managing, and payment solutions.
                    </p>
                    </div>
                    <div id="Personality" className="personality">
                    <p className="all-aboutt">Personality</p>
                    <h1 className="heading-4">I&#x27;m that designer who&#x27;s always questioning, always curious.</h1>
                    <p>
                        I love peeling back the layers of tough tech challenges and finding what makes them tick. It&#x27;s not just about thinking
                        outside the box – it&#x27;s about asking if we even need a box in the first place.
                    </p>
                    </div>
                    <div id="Family" className="family">
                    <p className="all-aboutt tagss">Family</p>
                    <h1 className="heading-5 all-aboutt">Home is in Lagos, Nigeria. (GMT+1)</h1>
                    <p>
                        Lagos is a city that never sleeps and keeps you on your toes. The vibrant markets, chaotic traffic, and unique challenges make
                        it an adventure like no other. With my other half – a brilliant mind in Education – and our son, who&#x27;s probably going to
                        out-game us all one day. We&#x27;re a lively bunch, always chatting about everything from Roblox strategies to space science.
                    </p>
                    <div className="cats-imgs">
                        <div className="cat-img"></div>
                        <div className="cat-img"></div>
                        <div className="cat-img"></div>
                        <div className="cat-img"></div>
                    </div>
                    </div>
                    <div id="Hobbies" className="hobbies">
                    <p className="tagss">Hobbies</p>
                    <h1 className="heading-6 all-aboutt">
                        Away from the intricacies of design, my world is filled with music and the exhilarating roar of my superbike engine.
                    </h1>
                    <p>
                        During the day, I'm fully immersed in the world of design, crafting user-centric experiences and sleek interfaces. However, as
                        soon as the workday concludes, my focus shifts to a lively blend of music and an adrenaline-fueled need for speed.
                        <br />
                    </p>
                    </div>
                </div>
                </div>
            </div>
            </section>

            <Footer />

    </div>
  )
}

export default About