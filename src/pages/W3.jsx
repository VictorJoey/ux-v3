import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'

function W3() {
  return (
    <div>
    <Header />
      <section className="blog-main-section">
  <div className="page-padding">
    <img
      alt=""
      loading="lazy"
      width={41}
      src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933d0d1aec033fa60b2614_659336f596a96e82e2d7a3ea_Group.png"
      className="blog-tag"
    />
    <h1 className="blog-main-heading">
      Orka Works scale-up initiative to accommodate the evolving needs of
      workers and the job market
    </h1>
    <p>
      Orka Work, a dynamic job platform, recently experienced a significant
      surge in user registrations, signaling a need for strategic scaling. The
      project focused on expanding into the hospitality sector and introducing
      the 'Switch' feature to facilitate easy access to a variety of job
      opportunities.
    </p>
    <div className="_3rd-role-others">
      <div id="w-node-_10af22ee-35d2-1ba9-d261-b80f0a66e5b3-1147350a">
        <div>
          <strong>Role</strong>
        </div>
        <div>Lead Product Designer</div>
      </div>
      <div id="w-node-b65bdaca-dd9c-5885-e832-1cd8d5cdf0ab-1147350a">
        <div>
          <strong>Timeline</strong>
        </div>
        <div>6 months, from Mar 2023 to Sept 2022</div>
      </div>
      <div id="w-node-_148d2ba2-ccfa-65e9-d667-153e0ff6f18e-1147350a">
        <div>
          <strong>Core Responsibilities</strong>
        </div>
        <div>
          System thinking, Research, Illustration/icons, Visual design, (UX)
          Assessment, User Segmentation Study,Market research, strategy planning
        </div>
      </div>
    </div>
  </div>
  <div className="top-img-2">
    <div id="w-node-_3c601a08-5a6b-64b7-2306-1852a3364cb9-1147350a">
      <img
        alt=""
        loading="lazy"
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933d0d1aec033fa60b262e_65924659bb045ff2082cf8d2_Sector%2520switch.png"
      />
    </div>
    <div id="w-node-_1c0623b3-5ccb-95d8-e806-ab41c4d8a406-1147350a">
      <img
        alt=""
        loading="lazy"
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933d0d1aec033fa60b2627_65933765d0c4400af2af6228_Group%252022740.png"
      />
    </div>
  </div>
  <div className="page-padding">
    <div className="all-cont">
      <div className="left-menu">
        <div
          data-animation="default"
          data-collapse="medium"
          data-duration={400}
          data-easing="ease"
          data-easing2="ease"
          role="banner"
          className="navbar-2 w-nav"
        >
          <div className="text-block-4 overview">Overview</div>
          <div className="container-2 w-container">
            <nav role="navigation" className="blog-menu w-nav-menu">
              <a
                href="w3#problems"
                className="nav-link w-nav-link"
              >
                Problem
              </a>
              <a
                href="w3#Goals"
                className="nav-link w-nav-link"
              >
                Goals
              </a>
              <a
                href="w3#solutions"
                className="nav-link w-nav-link"
              >
                Solution
              </a>
              <a
                href="w3#Results"
                className="nav-link w-nav-link"
              >
                Results
              </a>
              <div className="wrapper dfdf" />
              <p className="paragraph-18">Additional Context</p>
              <a
                href="w3#research"
                className="nav-link w-nav-link"
              >
                Research
              </a>
              <a
                href="w3#idianations"
                className="nav-link w-nav-link"
              >
                Ideation
              </a>
              <a
                href="w3#validations"
                className="nav-link w-nav-link"
              >
                Validation
              </a>
              <a
                href="w3#plan-of-action"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">Plan of Action</strong>
              </a>
              <a
                href="w3#future-steps"
                className="nav-link w-nav-link"
              >
                Future steps
              </a>
              <a
                href="w3#reflection"
                className="nav-link w-nav-link"
              >
                Reflection
              </a>
            </nav>
          </div>
        </div>
      </div>
      <div className="rigth-contt">
        <div id="problems" className="problrms">
          <div className="w-richtext">
            <p>Problem &amp; Insights</p>
            <h4>
              <strong>
                Workers encountered difficulties in applying for jobs across
                different sectors, primarily due to a confusing interface for
                switching between these sectors
              </strong>
            </h4>
            <p>
              Workers struggled to navigate and apply for jobs across different
              sectors due to a confusing interface and the aggregated
              presentation of sectors, complicating job searches. Additionally,
              the influx of user sign-ups highlighted the need for more diverse
              job opportunities to meet the growing demand.
            </p>
          </div>
          <div className="top--flexx">
            <div id="w-node-_6032ba81-2877-8c70-5b07-cddbecc1fdca-1147350a">
              <img
                alt=""
                loading="lazy"
                src="orka-works-scale-up-initiative-to-accommodate-the-evolving-needs-of-workers-and-the-job-market"
                className="w-dyn-bind-empty"
              />
            </div>
            <div id="w-node-_377ce8d6-1aa2-e9ed-10e5-4148d160e831-1147350a">
              <img
                alt=""
                loading="lazy"
                src="orka-works-scale-up-initiative-to-accommodate-the-evolving-needs-of-workers-and-the-job-market"
                className="w-dyn-bind-empty"
              />
            </div>
          </div>
        </div>
        <div id="Goals" className="goalsss">
          <div className="w-richtext">
            <p>Goals</p>
            <h4>
              <strong>
                Workers overwhelmingly selected the hospitality sector as their
                top preference, indicating a strong favourability for this
                industry
              </strong>
            </h4>
            <p>
              A key aspect of the project was the expansion into the hospitality
              sector, diversifying the range of job types and environments
              available on the platform. An equally important goal was to
              optimize the user experience, particularly by redesigning the
              interface to make navigation between different job sectors more
              intuitive and user-friendly, addressing the specific challenges
              introduced by the inclusion of the hospitality sector.
            </p>
            <h4>
              <strong>Incorporating the hospitality sector</strong>
            </h4>
            <p>
              the system has become significantly more organised and
              user-friendly, offering seamless navigation and an enhanced
              experience.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65924248725926bf5152447a_old.png"
                  alt=""
                />
              </div>
              <figcaption>
                <strong>
                  Removed multi sector selection and introducing new individual
                  sector signup flows
                </strong>
              </figcaption>
            </figure>
            <h4>
              <strong>Incorporating the hospitality sector</strong>
            </h4>
            <p>
              Streamlined the user experience by eliminating the sectors from
              filters and introduced new shift time options tailored
              specifically for the hospitality industry.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "3028pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592446de84d95473b66f957_new%20times.png"
                  alt=""
                />
              </div>
            </figure>
            <h4>
              <strong>
                Sector-switch feature to empower workers with the flexibility to
                apply seamlessly across various sectors, guided by data-driven
                insights and user feedback
              </strong>
            </h4>
            <p>
              I introduced a sector switch feature to empower workers with the
              flexibility to effortlessly apply to multiple jobs across various
              sectors. The initiative also placed a strong emphasis on
              data-driven decision-making, utilizing user feedback and analytics
              to inform the development process and guide feature enhancements.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "3028pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65924659bb045ff2082cf8d2_Sector%20switch.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="solutions" className="goalsss">
          <div className="w-richtext">
            <p>Solution</p>
            <h4>
              <strong>
                More jobs, scalable platform, seamless sector-switching, and a
                beacon for job seekers in a changing job market
              </strong>
            </h4>
            <p>
              Through a meticulous and iterative process that included user
              surveys, interviews, data analysis, wireframing, prototyping, and
              multiple rounds of sector-switch testing with a strong focus on
              enhancing the user experience, I spearheaded a significant
              initiative. This endeavor was a collaborative effort involving
              various teams, including Product Management, Engineering, Data
              Analysis, Quality Assurance, Content Management, Customer Support,
              and Marketing, all while ensuring alignment with our Chief
              Technology Officer (CTO).
            </p>
            <h4>
              <strong>Engaging User Interest in Hospitality Jobs </strong>
            </h4>
            <p>
              We actively sought user feedback to gauge interest in hospitality
              jobs, offering them the opportunity to express their interest and
              receive notifications about relevant job openings.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "3028pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65924895e02ba43ac6fe2350_Sector%20switch.png"
                  alt=""
                />
              </div>
            </figure>
            <h4>
              <strong>
                An expansive sector with diverse subcategories, offering
                abundant job options, effectively addresses the challenge of
                accommodating a large user base
              </strong>
            </h4>
            <p>
              The key outcome of this effort was the successful introduction of
              the hospitality sector to our platform. To achieve this, we
              prioritised scalability, implementing strategic measures to ensure
              the platform's adaptability to future expansion. Specifically, I
              designed the client portal with inherent scalability, laying the
              groundwork for seamless integration of additional sectors within
              the hospitality industry.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65924935fe20033ba13c7555_Orka%20Works%20candidate.png"
                  alt=""
                />
              </div>
            </figure>
            <h4>
              <strong>
                Shift workers are presented with alternative job options when
                their preferred one is unavailable
              </strong>
            </h4>
            <p>
              I implemented a feature for dynamic job suggestions. If a user
              sought a specific job, such as a security position, in their local
              area but none were available, our platform would proactively
              recommend alternative job opportunities, such as the 10 available
              hospitality positions. With a simple flick of the 'Switch,' users
              could seamlessly transition between sectors, obviating the need
              for tedious and time-consuming filtering processes that were
              previously in place.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "3028pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/6592516086f93610c27ea414_Sector%20swsuggseitch.png"
                  alt=""
                />
              </div>
            </figure>
            <h4>
              <strong>
                Filtering out ineligible users helps set realistic expectations
                among workers, ensuring that they are presented with job
                opportunities that align with their qualifications and
                preferences
              </strong>
            </h4>
            <p>
              I introduced a feature called
              <strong> Good Match for Orka </strong>to address a persistent
              challenge in our app - the presence of duplicate and irrelevant
              user profiles. This issue not only consumed valuable time but also
              led to user frustration. To remedy this, the "Good Match for Orka"
              feature incorporates a streamlined qualification process. Users
              are presented with four specific questions during the signup
              process. By answering these questions, they can determine their
              eligibility to sign up. Remarkably, this approach has resulted in
              a significant improvement, with approximately 35% of incorrect
              profiles being filtered out. In essence, Good Match for Orka
              serves as a powerful feature for setting better expectations among
              our workers, ensuring that the platform connects them with the
              most relevant opportunities while reducing the clutter of
              inaccurate user profiles.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "3028pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659252ced026cf54703b3e4a_godmatch.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Results" className="goalsss">
          <div className="w-richtext">
            <p>Results</p>
            <h4>
              <strong>
                90% increase in user satisfaction ratings, a 35% reduction in
                duplicated and incorrect user profiles, a 20% boost in user
                interactions, and a substantial client in the hospitality
                sector, contributing 500 new job listings to the platform.
              </strong>
            </h4>
            <p>
              Orka Work successfully secured a major client in the hospitality
              sector, marking a significant milestone in their expansion
              efforts. This client's partnership brought an impressive addition
              of new job listings to the platform from the hospitality sector
              alone.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65924a8340d0a805b1cfb574_reviews.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div className="wrapper" />
        <div id="research" className="researchs ideanation">
          <div className="w-richtext">
            <p>Research</p>
            <h4>
              <strong>
                Empathizing and understanding the pain points of the workers
              </strong>
            </h4>
            <p>
              As we delved deep into user interviews and research, we walked in
              the shoes of the workers, understanding their challenges,
              frustrations, and aspirations. We conducted a user segmentation
              and feedback study.
            </p>
            <p>
              <strong>Group 1 - Job Seekers</strong>: This group comprised
              individuals who had encountered difficulties in finding suitable
              job opportunities using our platform. We engaged with them to
              uncover the pain points, frustrations, and obstacles they faced
              during their job search experience.
            </p>
            <p>
              <strong>Group 2 - Registered Users</strong>: The second group
              consisted of individuals who were already registered with our
              platform. We aimed to gather feedback from them regarding their
              overall experience, as well as their preferences for additional
              sectors they wished to see on our platform.
            </p>
            <p>‍</p>
            <h4>
              <strong>
                Users voiced concerns about a scarcity of job opportunities,
                which occasionally resulted in periods of unemployment and posed
                financial challenges.
              </strong>
            </h4>
            <p>
              Our research journey included in-depth interviews and surveys with
              both groups. We asked probing questions to understand the specific
              pain points they encountered while using our app, ranging from
              interface navigation challenges to the availability of job
              listings on certain days. Notably, we identified that some users
              faced issues when trying to switch from one sector to another
              within the app. For instance, they mentioned that switching
              between sectors could be confusing at times, and on occasion, they
              couldn't find any job listings when attempting to explore
              different sectors.
            </p>
            <p>‍</p>
            <h4>
              <strong>
                We sought to expand our platform's offerings based on user
                preferences
              </strong>
            </h4>
            <p>
              Many respondents expressed a strong interest in job sectors such
              as waitering, assistant management, cooking, kitchen management
              and culinary arts. These insights directly informed our
              decision-making process, guiding us toward enhancing our platform
              to accommodate these desired sectors.
            </p>
          </div>
        </div>
        <div id="idianations" className="researchs ideant">
          <div className="w-richtext">
            <p>Ideation</p>
            <h4>
              <strong>
                Exploring sectors that can alleviate the challenge of users
                struggling to secure employment for their financial stability.
              </strong>
            </h4>
            <p>
              In our collaborative ideation sessions, involving the Project
              Manager and Business Analyst, we generated lists of sectors with
              the potential for expansion. Following in-depth market research
              and analysis of user feedback, we examined both worker-preferred
              sectors and sectors experiencing workforce shortages.
            </p>
            <p>‍</p>
            <h4>
              <strong>
                The creation of a singular, overarching sector encompasses a
                wide range of job opportunities
              </strong>
            </h4>
            <p>
              Based on our findings, we made the strategic decision to
              consolidate various job roles under one main sector called
              <strong>Hospitality</strong>. This main sector includes multiple
              sub-categories such as wine server, night club manager, line cook,
              kitchen manager, food and beverage service supervisor, chef, and
              bartender.
            </p>
            <p>‍</p>
            <p>
              During this race against the clock, I took on a multifaceted role,
              encompassing leadership within the design team and steering our
              overarching product strategy. My responsibilities spanned
              conducting comprehensive research and user interviews, guiding the
              development of visual and interaction design elements, and
              ensuring seamless coordination with our front-end development
              team.
            </p>
          </div>
        </div>
        <div id="validations" className="researchs ideant">
          <div className="w-richtext">
            <p>Validation</p>
            <h4>
              <strong>
                Involving shift workers in the process to create a solution that
                is better suited to their needs in order to ensure a positive
                and effective user experience
              </strong>
            </h4>
            <p>
              In our iterative User Acceptance Testing (UAT) phase, we engaged
              shift workers who willingly subscribed to notifications upon the
              introduction of the hospitality sector, conducting multiple rounds
              of testing. This strategic approach ensured that our testing pool
              was representative of the target user base and that our solution
              evolved based on continuous feedback. Through these repeated
              interactions, spanning several rounds, we collected robust user
              feedback, which subsequently emerged as the cornerstone of our
              assurance that the proposed solution, including the "Sector
              Switch" feature, would effectively address the problem at hand. It
              validated that our user-centric approach aligned with the needs
              and expectations of our users, making it a pivotal component of
              our solution's success.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "3028pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65924895e02ba43ac6fe2350_Sector%20switch.png"
                  alt=""
                />
              </div>
            </figure>
            <p>‍</p>
          </div>
        </div>
        <div id="plan-of-action" className="researchs ideant">
          <div className="w-richtext">
            <p>Plan of Action</p>
            <h4>
              <strong>
                Execution of the design, focusing on scalability to accommodate
                future sector additions and evolving client requirements
              </strong>
            </h4>
            <p>
              In collaboration with our front-end engineer, data analyst, and
              Product Manager, we successfully implemented this design. With
              valuable input from our CTO, I also ensured that the product was
              designed to be highly scalable, allowing for the seamless addition
              of new sectors to cater to our clients' evolving needs.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659219c5a52c8722dc7d1d03_Orka%20Works%20candidate.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="future-steps" className="researchs ideant">
          <div className="w-richtext">
            <p>Future steps</p>
            <h4>
              <strong>
                Prioritize user feedback, pursue further expansion, enhance
                personalization, and leverage data analytics to ensure platform
                effectiveness and user satisfaction
              </strong>
            </h4>
            <p>
              In the next phase of this project, we will maintain a strong focus
              on user feedback, aiming to continually refine the platform in
              response to evolving needs and preferences. Expansion remains a
              key goal, with potential exploration into additional sectors
              beyond hospitality and possible international expansion to tap
              into global markets. Enhancing personalization features,
              leveraging advanced data analytics, and ongoing collaboration with
              the marketing team will be pivotal in ensuring the platform's
              effectiveness and user satisfaction.
            </p>
          </div>
        </div>
        <div id="reflection" className="researchs ideant">
          <div className="w-richtext">
            <p>Reflection</p>
            <h4>
              <strong>
                Exploring sectors that can alleviate the challenge of users
                struggling to secure employment for their financial stability.
              </strong>
            </h4>
            <p>
              Orka Works scale-up initiative was not just another project for
              me; it was an exciting journey filled with purpose and the
              potential to make a meaningful impact on the lives of workers.
              Scaling up our platform to help workers find more job
              opportunities and ease their financial burdens was a mission I was
              genuinely enthusiastic about. One of the most rewarding aspects of
              this project was the opportunity to connect with workers on a
              deeper level. Conducting interviews and surveys to understand
              their pains, financial struggles, and aspirations brought a sense
              of empathy and purpose to our work. It was a humbling experience
              to hear firsthand about the challenges they faced and to know that
              we had the power to make a positive change in their lives.
            </p>
            <p>
              Introducing the hospitality sector and the "Sector Switch" feature
              was a moment of genuine excitement. It wasn't just about
              implementing features; it was about addressing real pain points.
              The idea for the sector switch feature, inspired by my personal
              experiences with job search platforms like Indeed, stemmed from a
              desire to simplify the job application process. Knowing that this
              feature would streamline the user experience and help workers
              navigate various job sectors brought a sense of accomplishment. I
              look forward to carrying these experiences and lessons into future
              projects aimed at improving the user experience and making a
              difference in the world.
            </p>
            <p>
              Furthermore, having a client abroad partner with us and witnessing
              the positive impact of our platform in bringing new job listings
              to our users was a source of great contentment. It highlighted the
              scalability and global potential of our project, reaffirming our
              commitment to our mission while simultaneously achieving our
              business goals.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65925059af078b167470ca40_Reflection.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<Footer />


    </div>
  )
}

export default W3