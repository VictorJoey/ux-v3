import React from 'react'
import Home from './pages/Home'
import About from './pages/About'
import Resume from './pages/Resume'
import W1 from './pages/W1'
import W2 from './pages/W2'
import W3 from './pages/W3'
import W4 from './pages/W4'
import W5 from './pages/W5'
import W6 from './pages/W6'

import { BrowserRouter, Routes, Route } from 'react-router-dom'


function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home/>}/>
      <Route path="/about" element={<About />} />
      <Route path="/resume" element={<Resume />} />
      <Route path="/w1" element={<W1 />} />
      <Route path="/w2" element={<W2 />} />
      <Route path="/w3" element={<W3 />} />
      <Route path="/w4" element={<W4 />} />
      <Route path="/w5" element={<W5 />} />
      <Route path="/w6" element={<W6 />} />
    </Routes>
    </BrowserRouter>
  );
}

export default App